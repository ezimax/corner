package LoginView;

public interface LoginView {

    void loginSuccess();
    void loginError();
    void loginValidation();
}
