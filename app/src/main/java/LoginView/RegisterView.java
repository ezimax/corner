package LoginView;

public interface RegisterView {
    void registerSuccess();
    void registerError();
    void registerValidation();
}
