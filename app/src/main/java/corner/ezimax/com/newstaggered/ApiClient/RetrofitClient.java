package corner.ezimax.com.newstaggered.ApiClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import corner.ezimax.com.newstaggered.singleton.ServerDetails;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(ServerDetails.getInstance().baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .client(provideOkHttpClient())
                    .build();
        }
        return retrofit;
    }
}
