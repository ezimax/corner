package corner.ezimax.com.newstaggered.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.BuildConfig;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import corner.ezimax.com.newstaggered.ApiClient.RetrofitClient;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.GroupMemberAdapter;
import corner.ezimax.com.newstaggered.fragment.ChatUsersList;
import corner.ezimax.com.newstaggered.fragment.CollectionFragment;
import corner.ezimax.com.newstaggered.fragment.LoginFragment;
import corner.ezimax.com.newstaggered.fragment.NavigationFragment;
import corner.ezimax.com.newstaggered.fragment.ProductFragment;
import corner.ezimax.com.newstaggered.fragment.UserProfile;
import corner.ezimax.com.newstaggered.interfaces.ApiInterface;
import corner.ezimax.com.newstaggered.singleton.ServerDetails;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationFragment.FragmentCall{

    private static final String TAG = "MainActivity";
    @BindView(R.id.home)
    LinearLayout home;
    @BindView(R.id.collection)
    LinearLayout collection;
    @BindView(R.id.group)
    ImageButton group;
    @BindView(R.id.chat)
    LinearLayout chat;
    @BindView(R.id.profile)
    LinearLayout profile;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.group_members_listview)
    ListView groupmemberslistview;
    private List<String> groupName;
    private int[] images = {R.drawable.smith_doe_avatar, R.drawable.jane_doe_avatar, R.drawable.john_doe_avatar, R.drawable.smith_doe_avatar, R.drawable.jane_doe_avatar, R.drawable.john_doe_avatar, R.drawable.test2};
    @BindView(R.id.bottom_navigation)
    LinearLayout bottomNavigation;
    private int group_member_count = 0;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);
        ButterKnife.bind(this);

        permissionCheck();
        groupMemberNameData();
    }

    private void permissionCheck() {
        try {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.CHANGE_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.WRITE_CONTACTS}, 1);
                log("onCreate if");
                Login();

            } else {
                Login();
                log("onCreate else");
            }

        } catch (Exception e) {
            log("onCreate catch" + e.getMessage());
        }
    }

    private void groupMemberNameData() {
        groupName = new ArrayList<>();
        groupName.add("Smith Doe");
        groupName.add("Jane Doe");
        groupName.add("John Doe");
        groupName.add("Smith Doe");
        groupName.add("Jane Doe");
        groupName.add("John Doe");
    }

    private void Login() {
//        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
//        apiInterface.getUserToken().enqueue(new Callback<Void>() {
//            @Override
//            public void onResponse(@NonNull retrofit2.Call<Void> call, @NonNull Response<Void> response) {
//                ServerDetails.getInstance().token = response.headers().get("x-access-token");
//
//            }
//
//            @Override
//            public void onFailure(@NonNull retrofit2.Call<Void> call, @NonNull Throwable t) {
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_container, new ProductFragment()).commit();
//        setBottomNavigationNotVisible();
    }

    private void log(String message) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, " ************* " + message);
    }

    @OnClick({R.id.home, R.id.collection, R.id.group, R.id.chat, R.id.profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.home:
                homeButtonFunctionality();
                groupMembersListClose();
                break;
            case R.id.collection:
                collectionButtonFunctionality();
                groupMembersListClose();
                break;
            case R.id.group:
                GroupMemberBubbleUI();
                break;
            case R.id.chat:
                chatButtonFunctionality();
                groupMembersListClose();
                break;
            case R.id.profile:
                profileButtonFunctionality();
                groupMembersListClose();
                break;
        }
    }

    private void profileButtonFunctionality() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new UserProfile()).addToBackStack(null).commit();
    }

    private void chatButtonFunctionality() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new ChatUsersList()).addToBackStack(null).commit();
    }

    private void collectionButtonFunctionality() {

        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionFragment(),"collectionFragment").addToBackStack(null).commit();
//        CollectionFragment collectionFragment = (CollectionFragment) getSupportFragmentManager().findFragmentByTag("collectionFragment");
//        collectionFragment.createNewCollectionButtonNotVisible();

//        CollectionFragment collectionFragment1 = new CollectionFragment();
//        collectionFragment1.createNewCollectionButtonNotVisible();

    }


    private void homeButtonFunctionality() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new ProductFragment()).addToBackStack(null).commit();
    }

    public void bottomNavigationVisibility() {
        bottomNavigation.setVisibility(View.VISIBLE);
    }

    public void setBottomNavigationNotVisible() {
        bottomNavigation.setVisibility(View.GONE);
    }

    public void GroupMemberBubbleUI() {
        group_member_count = ++group_member_count;
        if (group_member_count == 1) {
            groupmemberslistview.setVisibility(View.VISIBLE);
        }

        if (group_member_count == 2) {
            groupmemberslistview.setVisibility(View.GONE);
            group_member_count = 0;
        }
        groupmemberslistview.setCacheColorHint(this.getResources().getColor(R.color.tranparent_bg));
        GroupMemberAdapter groupMemberAdapter = new GroupMemberAdapter(this, groupName, images);
        groupmemberslistview.setDivider(this.getResources().getDrawable(R.drawable.transperent_color));
        groupmemberslistview.setDividerHeight(20);
        groupmemberslistview.setAdapter(groupMemberAdapter);
    }

    public void groupMembersListClose(){

        if(groupmemberslistview.getVisibility() == View.VISIBLE){
            groupmemberslistview.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        bottomNavigationVisibility();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();

        } else
            super.onBackPressed();
    }

    @Override
    public void call() {
        FragmentManager fm = getSupportFragmentManager();
        ProductFragment fragm = (ProductFragment) fm.findFragmentById(R.id.main_container);
        fragm.tagsVisible();
    }
}
