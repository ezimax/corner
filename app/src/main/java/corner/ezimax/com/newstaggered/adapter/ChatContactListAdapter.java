package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.fragment.ChatUsersList;
import de.hdodenhof.circleimageview.CircleImageView;

class ChatContactListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> myContactsName;
    private ArrayList<String> messages;
    private int[] myContactsImages;
    private ChatUsersList chatUsersList;
    private ArrayList<String> date_data;

    ChatContactListAdapter(Context context, ArrayList<String> myContactsName, int[] myContactsImages, ArrayList<String> messages, ChatUsersList chatUsersList, ArrayList<String> dateListData) {
        this.context = context;
        this.myContactsName = myContactsName;
        this.myContactsImages = myContactsImages;
        this.messages = messages;
        this.chatUsersList = chatUsersList;
        date_data = dateListData;
    }

    @Override
    public int getCount() {
        return myContactsName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.contacts_list_view_layout, null);
        TextView myChat_Contacts_Name = convertView.findViewById(R.id.myChat_Contacts_Name);
        CircleImageView myChat_Contacts_Avatar = convertView.findViewById(R.id.myChat_Contacts_Avatar);
        ImageView online_user_symbol = convertView.findViewById(R.id.online_user_symbol);
        TextView contacts_messages = convertView.findViewById(R.id.contacts_messages);
        TextView myChat_date = convertView.findViewById(R.id.myChat_day);
        LinearLayout my_chat_contacts_holder = convertView.findViewById(R.id.my_chat_contacts_holder);

        Typeface NunitoBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Bold.ttf");
        Typeface NunitoRegular_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Regular.ttf");

        myChat_Contacts_Name.setText(myContactsName.get(position));
        myChat_Contacts_Name.setTypeface(NunitoBold_TTF);
        myChat_Contacts_Avatar.setImageResource(myContactsImages[position]);
//        Picasso.get().load(myContactsImages[position]).placeholder(R.drawable.placeholder_image).into(myChat_Contacts_Avatar);
        contacts_messages.setText(messages.get(position));
        contacts_messages.setTypeface(NunitoRegular_TTF);
        myChat_date.setTypeface(NunitoRegular_TTF);
        myChat_date.setText(date_data.get(position));

        if (position == 0) {

            myChat_Contacts_Avatar.setPadding(10, 0, 0, 0);
            myChat_Contacts_Name.setPadding(10, 0, 0, 0);
            contacts_messages.setPadding(10, 0, 0, 0);

            contacts_messages.setTextColor(context.getResources().getColor(R.color.contact_message_color));
            myChat_date.setTextColor(context.getResources().getColor(R.color.contact_message_color));
            online_user_symbol.setVisibility(View.VISIBLE);
        }

        my_chat_contacts_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "position:" + position, Toast.LENGTH_SHORT).show();
                chatUsersList.switchToChatFragment();

            }
        });
        return convertView;
    }
}
