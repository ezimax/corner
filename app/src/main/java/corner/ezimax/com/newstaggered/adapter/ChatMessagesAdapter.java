package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import corner.ezimax.com.newstaggered.R;

public class ChatMessagesAdapter extends BaseAdapter {

    private Context context;
    private List<String> message;
    private List<String> MessageTimeResponse;
    private List<Boolean> initiatorResponse;
    private List<Integer> initiatorIdResponse;
    private List<Integer> recepientIdResponse;

    public ChatMessagesAdapter(Context context, List<String> message, List<String> messageTimeResponse, List<Boolean> initiatorResponse, List<Integer> initiatorIdResponse, List<Integer> recepientIdResponse) {
        this.context = context;
        this.message = message;
        MessageTimeResponse = messageTimeResponse;
        this.initiatorResponse = initiatorResponse;
        this.initiatorIdResponse = initiatorIdResponse;
        this.recepientIdResponse = recepientIdResponse;
    }

    @Override
    public int getCount() {
        return message.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Typeface GothamRoundedMedium_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/GothamRounded-Medium.otf");
        if (initiatorResponse.get(position)) {

            convertView = LayoutInflater.from(context).inflate(R.layout.chat_initiator_structure, null);
            TextView initiator_message = convertView.findViewById(R.id.initiator_message);
            TextView initiator_time = convertView.findViewById(R.id.initiator_time);

            if(message.get(position).length()<12){
                initiator_message.setGravity(Gravity.CENTER_HORIZONTAL);
            }else {
                initiator_message.setGravity(Gravity.START);
            }

            initiator_message.setText(message.get(position));
            initiator_time.setText(MessageTimeResponse.get(position));
            initiator_message.setTypeface(GothamRoundedMedium_TTF);
            initiator_time.setTypeface(GothamRoundedMedium_TTF);


        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.chat_participant_structure, null);
            TextView participant_message = convertView.findViewById(R.id.participant_message);
            TextView participant_time = convertView.findViewById(R.id.participant_time);

            if(message.get(position).length()<12){
                participant_message.setGravity(Gravity.CENTER_HORIZONTAL);
            }else {
                participant_message.setGravity(Gravity.START);
            }

            participant_message.setText(message.get(position));
            participant_time.setText(MessageTimeResponse.get(position));
            participant_message.setTypeface(GothamRoundedMedium_TTF);
            participant_time.setTypeface(GothamRoundedMedium_TTF);
        }
        return convertView;
    }


}
