package corner.ezimax.com.newstaggered.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.fragment.ChatUsersList;
import corner.ezimax.com.newstaggered.fragment.ShareImageToFriends;

public class ChatSwipeFragmentAdapter extends PagerAdapter {
    private ChatUsersList chatUsersList;
    private List<Integer> dataList;
    private Context context;
    private int maleImages[] = {R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136};
    private int femaleImages[] = {R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size};
    private ArrayList<String> myContactsName, messages;
    private RecyclerView ContactList_recycler_view;
    ArrayList<String> dateListData;

    public ChatSwipeFragmentAdapter(Context context, List<Integer> dataList, ChatUsersList chatUsersList) {
        this.context = context;
        this.dataList = dataList;
        this.chatUsersList = chatUsersList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view;
        staticData();
        if (dataList.get(position) == 0) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.my_chats_layout, container, false);
            TextView chat_heading = view.findViewById(R.id.chat_heading);

            Typeface CircularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
            chat_heading.setTypeface(CircularStdBold_TTF);
            ListView chatContactList = view.findViewById(R.id.chat_list_view);
            dateListData = new ArrayList<>();
            dateListData.add("04:30 pm");
            dateListData.add("FRI");
            dateListData.add("MON");
            dateListData.add("TUE");
            dateListData.add("FRI");
            dateListData.add("MON");
            dateListData.add("TUE");
            dateListData.add("FRI");
            dateListData.add("MON");
            dateListData.add("TUE");

            ChatContactListAdapter chatContactListAdapter = new ChatContactListAdapter(view.getContext(), myContactsName, maleImages, messages, chatUsersList,dateListData);
            chatContactList.setAdapter(chatContactListAdapter);
        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.my_contacts_layout, container, false);
            TextView contact_layout_heading = view.findViewById(R.id.contact_layout_heading);

            Typeface CircularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
            contact_layout_heading.setTypeface(CircularStdBold_TTF);

            ContactList_recycler_view = view.findViewById(R.id.contacts_list_view);

            initRecyclerView();
            ContactsListAdapter contactsListAdapter = new ContactsListAdapter(view.getContext(), femaleImages, myContactsName);
            ContactList_recycler_view.setAdapter(contactsListAdapter);

        }

        container.addView(view);
        return view;
    }

    private int dpToPx() {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }


    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context.getApplicationContext(), 2);
        ContactList_recycler_view.setLayoutManager(mLayoutManager);
        ContactList_recycler_view.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(), true));
        ContactList_recycler_view.setItemAnimator(new DefaultItemAnimator());
        shareFriendsImageAdapter adapter = new shareFriendsImageAdapter(context, myContactsName, femaleImages);
        ContactList_recycler_view.setAdapter(adapter);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    private void staticData() {
        myContactsName = new ArrayList<>();
        myContactsName.add("John Doe");
        myContactsName.add("Jane Doe");
        myContactsName.add("John Doe");
        myContactsName.add("Jane Doe");
        myContactsName.add("John Doe");
        myContactsName.add("Jane Doe");
        myContactsName.add("John Doe");
        myContactsName.add("Jane Doe");
        myContactsName.add("John Doe");
        myContactsName.add("Jane Doe");

        messages = new ArrayList<>();
        messages.add("Hey, how you doin?");
        messages.add("Sent you 10 photos");
        messages.add("Great! I’ll meet you there");
        messages.add("Where are you dude?");
        messages.add("Hey, how you doin?");
        messages.add("Sent you 10 photos");
        messages.add("Great! I’ll meet you there");
        messages.add("Where are you dude?");
        messages.add("Hey, how you doin?");
        messages.add("Sent you 10 photos");
    }


}
