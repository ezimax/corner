package corner.ezimax.com.newstaggered.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ViewHolder> {

    Context context;
    private int[] Image_urls;
    private ArrayList<String> images_name;

    ContactsListAdapter(Context context, int[] image_urls, ArrayList<String> images_name) {
        this.context = context;
        Image_urls = image_urls;
        this.images_name = images_name;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.image_url.setImageResource(Image_urls[position]);
//        Picasso.get().load(Image_urls[position]).placeholder(R.drawable.placeholder_image).into(holder.image_url);
        holder.nameList.setText(images_name.get(position));
    }

    @Override
    public int getItemCount() {
        return images_name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image_url;
        TextView nameList;

        public ViewHolder(View itemView) {
            super(itemView);
            image_url = itemView.findViewById(R.id.image_view_circular);
            nameList = itemView.findViewById(R.id.name_list_under_circular_image);

        }
    }
}
