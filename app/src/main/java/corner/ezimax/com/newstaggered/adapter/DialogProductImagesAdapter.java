package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import corner.ezimax.com.newstaggered.R;


public class DialogProductImagesAdapter extends BaseAdapter {
    private Context context;
    private static final int[] IMAGE_NAME1 = {R.drawable.main_image, R.drawable.second_main_sample, R.drawable.main_image, R.drawable.second_main_sample, R.drawable.main_image};

    public DialogProductImagesAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return IMAGE_NAME1.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.dialog_product_list_view_elements, null);
        ImageView img = convertView.findViewById(R.id.slider_image);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        img.setLayoutParams(layoutParams);
        img.setImageResource(IMAGE_NAME1[position]);

//        Picasso.get().load(IMAGE_NAME1[position]).placeholder(R.drawable.placeholder_image).into(img);

        return convertView;
    }
}
