package corner.ezimax.com.newstaggered.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corner.ezimax.com.newstaggered.R;

public class FollowerFollowingAdapter extends PagerAdapter {

    private List<Integer> dataList;
    private int[] maleImage = {R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136};
    private Context context;
    private int[] femaleImage = {R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size};
    private ArrayList<String> followingName, followersName;

    public FollowerFollowingAdapter(List<Integer> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view;

        if (dataList.get(position) == 0) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.following_swipable_layout, container, false);
            TextView following_layout_heading = view.findViewById(R.id.following_layout_heading);
            Typeface CircularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
            following_layout_heading.setTypeface(CircularStdBold_TTF);
            maleStaticData();
            RecyclerView following_RV = view.findViewById(R.id.following_recycler_view);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
            following_RV.setLayoutManager(mLayoutManager);
            following_RV.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(), true));
            following_RV.setItemAnimator(new DefaultItemAnimator());
            FollowingAdapter adapter = new FollowingAdapter(view.getContext(), followingName, maleImage);
            following_RV.setAdapter(adapter);

        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.followers_swipable_layout, container, false);
            TextView followers_layout_heading = view.findViewById(R.id.followers_layout_heading);
            Typeface CircularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
            followers_layout_heading.setTypeface(CircularStdBold_TTF);

            femaleStaticData();
            RecyclerView followers_RV = view.findViewById(R.id.followers_recycler_view);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
            followers_RV.setLayoutManager(mLayoutManager);
            followers_RV.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(), true));
            followers_RV.setItemAnimator(new DefaultItemAnimator());
            FollowerAdapter adapter = new FollowerAdapter(view.getContext(), followersName, femaleImage);
            followers_RV.setAdapter(adapter);

        }

        container.addView(view);
        return view;
    }

    private void femaleStaticData() {
        followersName = new ArrayList<>();
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
        followersName.add("Jane Doe");
    }

    private void maleStaticData() {
        followingName = new ArrayList<>();
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
        followingName.add("John Doe");
    }

    private int dpToPx() {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


}
