package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import corner.ezimax.com.newstaggered.R;
import de.hdodenhof.circleimageview.CircleImageView;


public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.ViewHolder> {

    private static final String TAG = "FriendsImageGridViewAdapter";
    private List<String> mNames ;
    private int[] mImageUrls;
    private Context mContext;

    FollowingAdapter(Context context, List<String> names, int[] imageUrls) {
        mNames = names;
        mImageUrls = imageUrls;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.following_layout_listitem, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Typeface NunitoBold_TTF = Typeface.createFromAsset(mContext.getAssets(), "fonts/Nunito-Bold.ttf");
        Typeface Nunito_ExtraBold_TTF = Typeface.createFromAsset(mContext.getAssets(), "fonts/Nunito-ExtraBold.ttf");

        holder.name.setText(mNames.get(position));
        holder.name.setTypeface(NunitoBold_TTF);
        holder.image.setImageResource(mImageUrls[position]);
//        Picasso.get().load(mImageUrls[position]).placeholder(R.drawable.placeholder_image).into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on an image: " + mNames.get(position));
                Toast.makeText(mContext, mNames.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        holder.unfollow.setTypeface(Nunito_ExtraBold_TTF);


    }

    @Override
    public int getItemCount() {
        return mImageUrls.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView name;
        Button unfollow;

        ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view_circular);
            name = itemView.findViewById(R.id.name_list_under_circular_image);
            unfollow = itemView.findViewById(R.id.unfollow_Button);
        }
    }
}
