package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import corner.ezimax.com.newstaggered.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class GroupMemberAdapter extends BaseAdapter {
    private Context context;
    private List<String> names;
    private int[] images;


    public GroupMemberAdapter(Context context, List<String> names, int[] images) {
        this.context = context;
        this.names = names;
        this.images = images;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.group_member_grid, null);
        Typeface NunitoBold_TTF = Typeface.createFromAsset(context.getAssets(),"fonts/Nunito-Bold.ttf");
        Typeface NunitoRegular_TTF = Typeface.createFromAsset(context.getAssets(),"fonts/Nunito-Regular.ttf");
        TextView groupMemberName =  convertView.findViewById(R.id.group_member_name);


        CircleImageView avatarImage =  convertView.findViewById(R.id.group_avatar_image);
        ImageView online_user_symbol =  convertView.findViewById(R.id.online_user_symbol_group);

        groupMemberName.setTypeface(NunitoRegular_TTF);
        groupMemberName.setText(names.get(position));
        if(position == 0){
            groupMemberName.setTypeface(NunitoBold_TTF);
            online_user_symbol.setVisibility(View.VISIBLE);
        }
        avatarImage.setImageResource(images[position]);
//        Picasso.get().load(images[position]).placeholder(R.drawable.placeholder_image).into(avatarImage);
        return convertView;
    }

}
