package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.fragment.DetailedProductInfoFragment;


public class HorizontalImagesAdapter extends RecyclerView.Adapter<HorizontalImagesAdapter.ViewHolder> {

    private int[] mImageUrls;
    private Context mContext;


    public HorizontalImagesAdapter(int[] mImageUrls, Context mContext) {
        this.mImageUrls = mImageUrls;
        this.mContext = mContext;
    }

    @Override
    public HorizontalImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(mContext).inflate(R.layout.collection_page_grid_items, null);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(HorizontalImagesAdapter.ViewHolder holder, int position) {
        holder.imageView.setImageResource(mImageUrls[position]);
//        Picasso.get().load(mImageUrls[position]).placeholder(R.drawable.placeholder_image).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) mContext;
                mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new DetailedProductInfoFragment()).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageUrls.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
        }
    }
}
