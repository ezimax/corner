package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.model.SearchPeopleModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleDataAdapter extends BaseAdapter {
    Context context;
    private ArrayList<SearchPeopleModel> sample_images_name;

    public PeopleDataAdapter(Context context, ArrayList<SearchPeopleModel> sample_images_name) {
        this.context = context;
        this.sample_images_name = sample_images_name;
    }

    @Override
    public int getCount() {
        return sample_images_name.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.people_data_listview_layout, null);
        CircleImageView people_list_Avatar = convertView.findViewById(R.id.people_list_Avatar);
        TextView people_Name_TV = convertView.findViewById(R.id.people_Name_TV);
        Typeface Nunito_semi_bold = Typeface.createFromAsset(context.getAssets(), "Nunito-SemiBold.ttf");
        people_Name_TV.setTypeface(Nunito_semi_bold);
        people_list_Avatar.setImageResource(sample_images_name.get(position).getPeopleImages());
        people_Name_TV.setText(sample_images_name.get(position).getPeopleName());
        return convertView;
    }

    public void setFilter(ArrayList<SearchPeopleModel> list) {
        sample_images_name = list;
        notifyDataSetChanged();
    }
}
