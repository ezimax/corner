package corner.ezimax.com.newstaggered.adapter;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.fragment.DetailedProductInfoFragment;
import corner.ezimax.com.newstaggered.model.ProductModel;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MasonryView> {

    private Context context;

    private int[] imgList = {R.drawable.new_sample_two, R.drawable.flipkat_one, R.drawable.jabong_sample_img1, R.drawable.new_sample_four,
            R.drawable.new_sample_five, R.drawable.jabong_sample_img3, R.drawable.new_sample_three, R.drawable.new_sample_two, R.drawable.flipkart_two,
            R.drawable.jabong_sample_img2};

    private int[] avatarList = {R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size,
            R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size,
            R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size};

    private String[] nameList = {"John Doe", "Jane Doe", "Jane Doe", "Jane Doe", "John Doe", "Jane Doe",
            "John Doe", "Jane Doe", "John Doe", "Jane Doe"};
    ArrayList<ProductModel> personNames;

    public ProductAdapter(Context context, ArrayList<ProductModel> personNames) {
        this.context = context;
        this.personNames = personNames;
    }
    @Override
    public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_page_grid_items, parent, false);
        return new MasonryView(layoutView);
    }

    @Override
    public void onBindViewHolder(MasonryView holder, int position) {
        holder.imageView.setImageResource(imgList[position]);
        holder.avatar_image.setImageResource(avatarList[position]);
//        Picasso.get().load(imgList[position]).placeholder(R.drawable.placeholder_image).into(holder.imageView);
//        Picasso.get().load(avatarList[position]).placeholder(R.drawable.placeholder_image).into(holder.avatar_image);
        holder.designer_name.setText(personNames.get(position).getProductName());

        Typeface nunitoBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Bold.ttf");
        holder.designer_name.setTypeface(nunitoBold_TTF);

        Typeface nunitoRegular_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Regular.ttf");
        holder.product_date.setTypeface(nunitoRegular_TTF);


//        holder.home_page_product_price.setTypeface(nunitoBold_TTF);

//        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                Toast.makeText(context, "long press", Toast.LENGTH_SHORT).show();
//
//                return false;
//            }
//        });

        holder.share_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MainActivity) context).GroupMemberBubbleUI();
            }
        });



        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) context;
                mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new DetailedProductInfoFragment()).addToBackStack(null).commit();
            }
        });
    }


    @Override
    public int getItemCount() {
        return personNames.size();
    }

    class MasonryView extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView, share_image;
        TextView designer_name, product_date, home_page_product_price;
        CircleImageView avatar_image;

        MasonryView(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageView = itemView.findViewById(R.id.img);
            share_image = itemView.findViewById(R.id.share_image);
            avatar_image = itemView.findViewById(R.id.avatar_image);
            designer_name = itemView.findViewById(R.id.designer_name);
            product_date = itemView.findViewById(R.id.product_date);
//            home_page_product_price = itemView.findViewById(R.id.home_page_product_price);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Toast.makeText(context, "position :" + position, Toast.LENGTH_SHORT).show();
        }
    }

//    public void setFilter(ArrayList<ProductModel> list) {
//        personNames = list;
//        notifyDataSetChanged();
//    }
}
