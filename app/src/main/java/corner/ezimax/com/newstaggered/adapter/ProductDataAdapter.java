package corner.ezimax.com.newstaggered.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.model.SearchProductsModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProductDataAdapter extends BaseAdapter {
    Context context;
    private int[] sample_image_url;
    private ArrayList<SearchProductsModel> sample_images_name;
    ArrayList<String> sample_image_price;

    public ProductDataAdapter(Context context, ArrayList<SearchProductsModel> sample_images_name, ArrayList<String> sample_image_price) {
        this.context = context;
        this.sample_images_name = sample_images_name;
        this.sample_image_price = sample_image_price;
    }

    @Override
    public int getCount() {
        return sample_images_name.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.people_product_listview_layout, null);
        CircleImageView products_list_Avatar = convertView.findViewById(R.id.products_list_Avatar);
        TextView products_Name_TV = convertView.findViewById(R.id.products_Name_TV);
        TextView products_price_TV = convertView.findViewById(R.id.products_price_TV);

        Typeface Nunito_semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-SemiBold.ttf");
        Typeface Nunito_regular = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Regular.ttf");

        products_list_Avatar.setImageResource(sample_images_name.get(position).getProductImages());
        products_Name_TV.setText(sample_images_name.get(position).getProductName());
        products_Name_TV.setTypeface(Nunito_semi_bold);
        products_price_TV.setTypeface(Nunito_regular);
        products_price_TV.setText(sample_image_price.get(position));
        return convertView;
    }

    public void setFilter(ArrayList<SearchProductsModel> list) {
        sample_images_name = list;
        notifyDataSetChanged();
    }
}
