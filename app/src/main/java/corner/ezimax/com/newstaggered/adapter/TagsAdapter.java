package corner.ezimax.com.newstaggered.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.MyViewHolder> {
    private ArrayList<String> personNames;
    int[] personImages;
    Context context;
    private int row_index = -1;

    public TagsAdapter(Context context, ArrayList personNames) {
        this.context = context;
        this.personNames = personNames;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_rowlayout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        if(position == 0){
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                holder.tags_holder.setBackground(context.getResources().getDrawable(R.drawable.switch_background_color));
//            }
//            holder.name.setTextColor(Color.WHITE);
//        }

        holder.tags_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
            }
        });
        if (row_index == position) {

            holder.tags_holder.setBackground(context.getResources().getDrawable(R.drawable.switch_background_color));
            holder.name.setTextColor(Color.WHITE);
        }else {
            holder.tags_holder.setBackground(context.getResources().getDrawable(R.drawable.background_color));
            holder.name.setTextColor(context.getResources().getColor(R.color.tags_color));
        }
        holder.name.setText(personNames.get(position));
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Bold.ttf");
        holder.name.setTypeface(typeface);


    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        RelativeLayout tags_holder;

        MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            tags_holder = itemView.findViewById(R.id.tags_holder);
        }
    }
}