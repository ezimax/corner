package corner.ezimax.com.newstaggered.adapter.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;


public class ColorAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> colorName;
    private String filter_checkbox_id_prefix = "111";

    public ColorAdapter(Context context, ArrayList<String> colorName) {
        this.context = context;
        this.colorName = colorName;
    }

    @Override
    public int getCount() {
        return colorName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.navigation_color_adapter_layout, null);
        final View convertView1 = convertView;
        final TextView colorNavigationTV = convertView.findViewById(R.id.color_navigation_TV);
        final ImageView color_checked_status = convertView.findViewById(R.id.color_checked_status);
        final ImageView textFrontColor = convertView.findViewById(R.id.text_front_color_View);

        Typeface circularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");


        color_checked_status.setId(Integer.valueOf(filter_checkbox_id_prefix + String.valueOf(position)));
        LinearLayout color_strip_holder = convertView.findViewById(R.id.color_strip_holder);
        colorNavigationTV.setText(colorName.get(position));
        colorNavigationTV.setTypeface(circularStdBold_TTF);
        int[] rainbow = context.getResources().getIntArray(R.array.rainbow);
        colorNavigationTV.setTextColor(rainbow[position]);

        int circleImage[] = {R.drawable.black_circle_vector, R.drawable.red_circle_vector, R.drawable.blue_circle_vector, R.drawable.green_circle_vector, R.drawable.yellow_circle_vector, R.drawable.purple_circle_vector};

        textFrontColor.setImageDrawable(convertView.getResources().getDrawable(circleImage[position]));
        color_strip_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView color_CheckBox_Image = convertView1.findViewById(Integer.valueOf(filter_checkbox_id_prefix + String.valueOf(position)));
                if (color_CheckBox_Image.getVisibility() == View.VISIBLE) {
                    color_CheckBox_Image.setVisibility(View.GONE);
                } else if (color_CheckBox_Image.getVisibility() == View.GONE) {
                    color_CheckBox_Image.setVisibility(View.VISIBLE);
                }
            }
        });
        return convertView;
    }

}
