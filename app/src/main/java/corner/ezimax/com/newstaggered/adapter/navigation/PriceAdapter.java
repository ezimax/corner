package corner.ezimax.com.newstaggered.adapter.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;

public class PriceAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> priceList;
    private String filter_checkbox_id_prefix = "333";
    private final ImageView[] lastImageview = new ImageView[1];

    public PriceAdapter(Context context, ArrayList<String> priceList) {
        this.context = context;
        this.priceList = priceList;
    }

    @Override
    public int getCount() {
        return priceList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.navigation_price_adapter_layout, null);
        final View convertView1 = convertView;
        TextView price_navigation_TV = convertView.findViewById(R.id.price_navigation_TV);
        ImageView price_checked_status = convertView.findViewById(R.id.price_checked_status);
        RelativeLayout price_strip_holder = convertView.findViewById(R.id.price_strip_holder);
        price_navigation_TV.setText(priceList.get(position));

        Typeface circularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
        price_navigation_TV.setTypeface(circularStdBold_TTF);

        price_checked_status.setId(Integer.parseInt(filter_checkbox_id_prefix + String.valueOf(position)));
        price_strip_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView price_CheckBox_Image = convertView1.findViewById(Integer.valueOf(filter_checkbox_id_prefix + String.valueOf(position)));
                if (price_CheckBox_Image.getVisibility() == View.VISIBLE) {
                    price_CheckBox_Image.setVisibility(View.GONE);
                } else if (price_CheckBox_Image.getVisibility() == View.GONE) {
                    price_CheckBox_Image.setVisibility(View.VISIBLE);
                    if (lastImageview[0] != null) {
                        if (lastImageview[0].getVisibility() == View.VISIBLE) {
                            lastImageview[0].setVisibility(View.GONE);
                        }
                    }
                }
                lastImageview[0] = price_CheckBox_Image;
            }
        });
        return convertView;
    }
}
