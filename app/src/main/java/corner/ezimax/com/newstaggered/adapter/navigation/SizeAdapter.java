package corner.ezimax.com.newstaggered.adapter.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import corner.ezimax.com.newstaggered.R;


public class SizeAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> sizeNameList;
    private String filter_color_checkbox_prefix = "222";

    public SizeAdapter(Context context, ArrayList<String> sizeNameList) {
        this.context = context;
        this.sizeNameList = sizeNameList;
    }

    @Override
    public int getCount() {
        return sizeNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.navigation_size_adapter_layout, null);
        TextView sizeNavigationTV = convertView.findViewById(R.id.size_navigation_TV);
        ImageView sizeCheckedStatus = convertView.findViewById(R.id.size_checked_status);
        RelativeLayout sizeStripHolder = convertView.findViewById(R.id.size_strip_holder);

        Typeface circularStdBold_TTF = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");


        final View convertView1 = convertView;
        sizeNavigationTV.setText(sizeNameList.get(position));
        sizeNavigationTV.setTypeface(circularStdBold_TTF);
        sizeCheckedStatus.setId(Integer.parseInt(filter_color_checkbox_prefix + String.valueOf(position)));

        sizeStripHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView sizeCheckBoxImage = convertView1.findViewById(Integer.valueOf(filter_color_checkbox_prefix + String.valueOf(position)));
                if (sizeCheckBoxImage.getVisibility() == View.VISIBLE) {
                    sizeCheckBoxImage.setVisibility(View.GONE);
                } else if (sizeCheckBoxImage.getVisibility() == View.GONE) {
                    sizeCheckBoxImage.setVisibility(View.VISIBLE);
                }
            }
        });
        return convertView;
    }

}
