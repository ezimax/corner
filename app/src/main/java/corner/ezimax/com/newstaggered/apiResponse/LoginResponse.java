package corner.ezimax.com.newstaggered.apiResponse;

public class LoginResponse {


    /**
     * auth : true
     * token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjE2Mjk0OTcsImV4cCI6MTUyMTcxNTg5N30.zRpI2i2einfb6BjS-GE9IVnJD_jJ2KhD9-VHvsVaBp4
     * message : user logged In successfully
     * result : {"_id":"5ab20b7a5376084844036bfd","lastName":"solanki","emailId":"aaa@ss.com","firstName":"kapil","mobile":"9599743364","middleName":"","password":"$2a$08$zPlhaFrFmyfDKT0TYIPNb.PbdEbdbz/jQtdnqjFgFAe.PVWsIcwH.","userId":"191263607210318","__v":0}
     */

    private boolean auth;
    private String token;
    private String message;
    private ResultBean result;

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * _id : 5ab20b7a5376084844036bfd
         * lastName : solanki
         * emailId : aaa@ss.com
         * firstName : kapil
         * mobile : 9599743364
         * middleName :
         * password : $2a$08$zPlhaFrFmyfDKT0TYIPNb.PbdEbdbz/jQtdnqjFgFAe.PVWsIcwH.
         * userId : 191263607210318
         * __v : 0
         */

        private String _id;
        private String lastName;
        private String emailId;
        private String firstName;
        private String mobile;
        private String middleName;
        private String password;
        private String userId;
        private int __v;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }
    }
}
