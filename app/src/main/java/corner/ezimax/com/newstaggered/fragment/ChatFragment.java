package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.adapter.ChatMessagesAdapter;


public class ChatFragment extends Fragment {

    @BindView(R.id.chat_back_arrow)
    ImageView chatBackArrow;
    Unbinder unbinder;
    @BindView(R.id.chat_user_name)
    TextView chatUserName;
    @BindView(R.id.product_category)
    TextView productCategory;
    @BindView(R.id.product_category_also)
    TextView productCategoryAlso;
    @BindView(R.id.gender_type)
    TextView genderType;
    @BindView(R.id.product_price)
    TextView product_price;
    Typeface NunitoBold_TTF, GothamRoundedMedium_TTF, GothamRoundedBook_TTF;
    @BindView(R.id.type_message_ET)
    EditText type_message_ET;
    JSONArray jsonArray;
    @BindView(R.id.chat_messages_listView)
    ListView chatMessagesListView;
    JSONObject chatObject;
    List<String> message;
    List<String> MessageTimeResponse;
    List<Boolean> initiatorResponse;
    List<Integer> initiatorIdResponse;
    List<Integer> recepientIdResponse;
    @BindView(R.id.send_message_button)
    ImageView sendMessageButton;
    private ChatMessagesAdapter chatMessagesAdapter;
    private String sent_message;
    private String date;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.chat_layout, null);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        unbinder = ButterKnife.bind(this, view);
        applyfonts();
        jsonData();
        return view;
    }

    @SuppressLint("LongLogTag")
    private void jsonData() {
        jsonArray = new JSONArray();

        JSONObject initiator = new JSONObject();
        try {
            initiator.put("Initiator", true);
            initiator.put("InitiatorId", 1);
            initiator.put("RecepientId", 1);
            initiator.put("MessageText", "Hi, is this still available?");
            initiator.put("MessageTime", "08:00 PM");
            initiator.put("MessageImage", "");
            initiator.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject participant = new JSONObject();
        try {
            participant.put("Initiator", false);
            participant.put("InitiatorId", 2);
            participant.put("RecepientId", 2);
            participant.put("MessageText", "Hi, sorry for late reply. Yes it’s still available.");
            participant.put("MessageTime", "09:40 PM");
            participant.put("MessageImage", "");
            participant.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject initiator1 = new JSONObject();
        try {
            initiator1.put("Initiator", true);
            initiator1.put("InitiatorId", 3);
            initiator1.put("RecepientId", 3);
            initiator1.put("MessageText", "Nice!!");
            initiator1.put("MessageTime", "10:00 PM");
            initiator1.put("MessageImage", "");
            initiator1.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsonArray.put(initiator);
        jsonArray.put(participant);
        jsonArray.put(initiator1);

        chatObject = new JSONObject();
        try {
            chatObject.put("chatMessage", jsonArray);

            message = new ArrayList<>();
            MessageTimeResponse = new ArrayList<>();
            initiatorResponse = new ArrayList<>();
            initiatorIdResponse = new ArrayList<>();
            recepientIdResponse = new ArrayList<>();
            JSONArray jsonArray1 = null;
            try {
                jsonArray1 = chatObject.getJSONArray("chatMessage");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsonArray1 != null) {
                for (int i = 0; i < jsonArray1.length(); i++) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = jsonArray1.getJSONObject(i);

                        Boolean initiatorData = jsonObject.getBoolean("Initiator");
                        initiatorResponse.add(initiatorData);
                        Integer initiatorID = jsonObject.getInt("InitiatorId");
                        initiatorIdResponse.add(initiatorID);
                        Integer RecepientId = jsonObject.getInt("RecepientId");
                        recepientIdResponse.add(RecepientId);
                        String MessageText = jsonObject.getString("MessageText");
                        message.add(MessageText);
                        String MessageTime = jsonObject.getString("MessageTime");
                        MessageTimeResponse.add(MessageTime);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        chatMessagesAdapter = new ChatMessagesAdapter(getContext(), message, MessageTimeResponse, initiatorResponse, initiatorIdResponse, recepientIdResponse);
        chatMessagesListView.setAdapter(chatMessagesAdapter);
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        GothamRoundedMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRounded-Medium.otf");

        chatUserName.setTypeface(NunitoBold_TTF);
        type_message_ET.setTypeface(GothamRoundedMedium_TTF);
        productCategory.setTypeface(GothamRoundedMedium_TTF);
        productCategoryAlso.setTypeface(GothamRoundedMedium_TTF);
        genderType.setTypeface(GothamRoundedBook_TTF);
        product_price.setTypeface(GothamRoundedMedium_TTF);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.chat_back_arrow, R.id.send_message_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chat_back_arrow:
                ((MainActivity) getActivity()).bottomNavigationVisibility();
                getFragmentManager().beginTransaction().replace(R.id.main_container, new ChatUsersList()).addToBackStack(null).commit();
                break;
            case R.id.send_message_button:

                sent_message = type_message_ET.getText().toString();
                type_message_ET.setText("");
                type_message_ET.setHint(R.string.type_here);
                @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("h:mm a");
                date = df.format(Calendar.getInstance().getTime());
                if (message.size() == initiatorResponse.size() && initiatorResponse.size() == initiatorIdResponse.size() && initiatorIdResponse.size() == MessageTimeResponse.size()
                        ) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (sent_message.matches("")) {
                                Toast.makeText(getActivity(), "Enter message please", Toast.LENGTH_SHORT).show();
                            } else {
                                message.add(sent_message);
                                initiatorResponse.add(true);
                                initiatorIdResponse.add(3);
                                MessageTimeResponse.add(date);
                                chatMessagesAdapter.notifyDataSetChanged();
                            }

                        }
                    });
                }
                break;
        }
    }

}
