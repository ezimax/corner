package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.Utils.SpacesItemDecoration;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.adapter.HorizontalImagesAdapter;
import corner.ezimax.com.newstaggered.adapter.ProductAdapter;
import corner.ezimax.com.newstaggered.model.ProductModel;
import de.hdodenhof.circleimageview.CircleImageView;


public class CollectionFragment extends Fragment {

    @BindView(R.id.User_Avatar)
    CircleImageView UserAvatar;
    @BindView(R.id.user_full_name)
    TextView userFullName;
    @BindView(R.id.user_location)
    TextView userLocation;
    @BindView(R.id.user_description)
    TextView userDescription;
    @BindView(R.id.user_collection_count)
    TextView userCollectionCount;
    @BindView(R.id.user_followers_count)
    TextView userFollowersCount;
    @BindView(R.id.user_following_count)
    TextView userFollowingCount;
    @BindView(R.id.casual_collection_products)
    RecyclerView casual_collection_products;
    @BindView(R.id.formal_collection_products)
    RecyclerView formal_collection_products;
    Unbinder unbinder;
    @BindView(R.id.repost_button)
    LinearLayout repostButton;
    @BindView(R.id.collection_button)
    LinearLayout collectionButton;
    @BindView(R.id.repost_grid)
    RecyclerView repostGrid;
    @BindView(R.id.collection_elements)
    LinearLayout collectionElements;
    @BindView(R.id.collection_tab_content)
    LinearLayout collectionTabContent;
    @BindView(R.id.repost_tab_content)
    LinearLayout repostTabContent;
    @BindView(R.id.edit_profile_image)
    ImageView editProfileImage;
    @BindView(R.id.lock_icon_imageview)
    ImageView lock_icon_imageview;
    @BindView(R.id.followings_value)
    LinearLayout followingsValue;
    @BindView(R.id.followings_text)
    LinearLayout followingsText;

    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, NunitoExtraBold_TTF;
    @BindView(R.id.followers_value)
    LinearLayout followersValue;
    @BindView(R.id.user_collection_TV)
    TextView userCollectionTV;
    @BindView(R.id.user_followers_TV)
    TextView userFollowersTV;
    @BindView(R.id.user_following_TV)
    TextView userFollowingTV;
    @BindView(R.id.repost_button_TV)
    TextView repostButtonTV;
    @BindView(R.id.collection_button_TV)
    TextView collectionButtonTV;
    @BindView(R.id.create_new_collection_TV)
    TextView createNewCollectionTV;
    @BindView(R.id.casual_text_TV)
    TextView casualTextTV;
    @BindView(R.id.formal_text_TV)
    TextView formalTextTV;
    @BindView(R.id.create_new_collection_holder_CV)
    CardView create_new_collection_holder_LL;
    ArrayList<ProductModel> productModels = new ArrayList<>();


    private int[] imgList = {R.drawable.new_sample_three, R.drawable.new_sample_two, R.drawable.new_sample_one, R.drawable.new_sample_five,
            R.drawable.jabong_sample_img1, R.drawable.new_sample_three, R.drawable.jabong_male_sample_img, R.drawable.jabong_sample_img2,
            R.drawable.jabong_sample_img3, R.drawable.new_sample_three};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.user_collection_layout, null);
        unbinder = ButterKnife.bind(this, view);
        editProfileImage.setImageResource(R.drawable.chat_icon_vector);
        create_new_collection_holder_LL.setVisibility(View.GONE);
        return view;
    }

    private void modelData() {
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.flipkat_one)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.jabong_sample_img1)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_four)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_five)));


        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img3)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_three)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.flipkart_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img2)));

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        modelData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        casual_collection_products.setLayoutManager(layoutManager);
        HorizontalImagesAdapter adapter1 = new HorizontalImagesAdapter(imgList, getActivity());
        casual_collection_products.setAdapter(adapter1);


        formal_collection_products.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        formal_collection_products.setLayoutManager(layoutManager1);
        HorizontalImagesAdapter adapter3 = new HorizontalImagesAdapter(imgList, getActivity());
        formal_collection_products.setAdapter(adapter3);


        repostGrid.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        ProductAdapter adapter2 = new ProductAdapter(getActivity(),productModels);
        repostGrid.setAdapter(adapter2);
        SpacesItemDecoration decoration2 = new SpacesItemDecoration(16);
        repostGrid.addItemDecoration(decoration2);
        applyfonts();
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        userFullName.setTypeface(NunitoBold_TTF);

        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        userLocation.setTypeface(NunitoRegular_TTF);

        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        userDescription.setTypeface(NunitoLight_TTF);


        userCollectionCount.setTypeface(NunitoBold_TTF);
        userFollowersCount.setTypeface(NunitoBold_TTF);
        userFollowingCount.setTypeface(NunitoBold_TTF);


        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        userCollectionTV.setTypeface(NunitoExtraBold_TTF);
        userFollowersTV.setTypeface(NunitoExtraBold_TTF);
        userFollowingTV.setTypeface(NunitoExtraBold_TTF);

        repostButtonTV.setTypeface(NunitoRegular_TTF);
        collectionButtonTV.setTypeface(NunitoRegular_TTF);

        createNewCollectionTV.setTypeface(NunitoBold_TTF);
        casualTextTV.setTypeface(NunitoBold_TTF);
        formalTextTV.setTypeface(NunitoBold_TTF);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Optional
    @OnClick({R.id.repost_button, R.id.collection_button, R.id.edit_profile_image, R.id.lock_icon_imageview, R.id.followings_text, R.id.followings_value, R.id.followers_value, R.id.followers_text, R.id.lock_icon_imageview1, R.id.collection_settings_image, R.id.collection_settings_image_2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.repost_button:
                collectionTabContent.setVisibility(View.GONE);
                repostTabContent.setVisibility(View.VISIBLE);
                break;
            case R.id.collection_button:
                collectionTabContent.setVisibility(View.VISIBLE);
                repostTabContent.setVisibility(View.GONE);
                break;
            case R.id.edit_profile_image:
                ((MainActivity) getActivity()).setBottomNavigationNotVisible();
                getFragmentManager().beginTransaction().replace(R.id.main_container, new ChatFragment()).addToBackStack(null).commit();
                break;
            case R.id.lock_icon_imageview:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionSettings()).addToBackStack(null).commit();
                break;
            case R.id.followings_value:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followings_text:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followers_value:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followers_text:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.collection_settings_image:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionSettings()).addToBackStack(null).commit();
                break;
            case R.id.collection_settings_image_2:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionSettings()).addToBackStack(null).commit();
                break;
        }
    }

}
