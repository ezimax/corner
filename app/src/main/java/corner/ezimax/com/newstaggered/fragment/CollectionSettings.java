package corner.ezimax.com.newstaggered.fragment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.FriendsImageGridViewAdapter;
import corner.ezimax.com.newstaggered.adapter.GroupImageHorizontalScrollAdapter;

public class CollectionSettings extends Fragment {
    @BindView(R.id.private_text)
    TextView privateText;
    @BindView(R.id.switch_toggle_btn)
    SwitchButton switchToggleBtn;
    @BindView(R.id.invite_btn)
    TextView inviteBtn;
    @BindView(R.id.group_name_list_recyclerView)
    RecyclerView groupNameListRecyclerView;
    List<String> groupNames, friendsName;

    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, CircularStdBold_TTF, CircularStdMedium_TTF;
    int[] imagurl = {R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136};
    Unbinder unbinder;
    @BindView(R.id.friends_image_list_recyclerView)
    RecyclerView friendsImageListRecyclerView;
    int[] friendsImage = {R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size, R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size, R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size,R.drawable.male_big_avatar_116x136, R.drawable.female_avatar_big_size, R.drawable.male_big_avatar_116x136};
    @BindView(R.id.collection_setting_heading_TV)
    TextView collectionSettingHeadingTV;
    @BindView(R.id.group_heading_TV)
    TextView groupHeadingTV;
    @BindView(R.id.friends_heading_TV)
    TextView friendsHeadingTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.collection_settings_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        applyfonts();
        return view;
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        CircularStdBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Bold.otf");
        CircularStdMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");

        collectionSettingHeadingTV.setTypeface(CircularStdBold_TTF);
        groupHeadingTV.setTypeface(NunitoBold_TTF);
        privateText.setTypeface(NunitoBold_TTF);
        inviteBtn.setTypeface(NunitoBold_TTF);
        friendsHeadingTV.setTypeface(NunitoBold_TTF);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        staticData();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        groupNameListRecyclerView.setLayoutManager(layoutManager);
        GroupImageHorizontalScrollAdapter adapter = new GroupImageHorizontalScrollAdapter(getActivity(), groupNames, imagurl);
        groupNameListRecyclerView.setAdapter(adapter);

        initRecyclerView();

    }

    private void staticData() {
        groupNames = new ArrayList<>();
        groupNames.add("Group 1");
        groupNames.add("Group 2");
        groupNames.add("Group 3");
        groupNames.add("Group 4");
        groupNames.add("Group 5");
        groupNames.add("Group 6");
        groupNames.add("Group 7");
        groupNames.add("Group 8");
        groupNames.add("Group 9");
        groupNames.add("Group 10");

        friendsName = new ArrayList<>();
        friendsName.add("Smith Doe");
        friendsName.add("Ren Doe");
        friendsName.add("Jane Doe");
        friendsName.add("Smith Doe");
        friendsName.add("Ren Doe");
        friendsName.add("Jane Doe");
        friendsName.add("Smith Doe");
        friendsName.add("Ren Doe");
        friendsName.add("Jane Doe");
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        friendsImageListRecyclerView.setLayoutManager(mLayoutManager);
        friendsImageListRecyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(), true));
        friendsImageListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        FriendsImageGridViewAdapter adapter = new FriendsImageGridViewAdapter(getActivity(), friendsName, friendsImage);
        friendsImageListRecyclerView.setAdapter(adapter);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx() {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
