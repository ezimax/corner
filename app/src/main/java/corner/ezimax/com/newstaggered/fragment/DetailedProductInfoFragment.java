package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;


public class DetailedProductInfoFragment extends Fragment {

    @BindView(R.id.imagemain)
    ImageView imagemain;
    @BindView(R.id.imagev1)
    ImageView imagev1;
    @BindView(R.id.imagev2)
    ImageView imagev2;
    @BindView(R.id.imagev3)
    ImageView imagev3;
    @BindView(R.id.imagev4)
    ImageView imagev4;
    @BindView(R.id.scrolbar1)
    ScrollView scrolbar1;
    Unbinder unbinder;
    @BindView(R.id.avatar_image)
    CircleImageView avatarImage;
    @BindView(R.id.share_icon)
    ImageView shareIcon;
    @BindView(R.id.imagemain_cv)
    CardView imagemainCv;
    @BindView(R.id.message_count)
    TextView messageCount;
    @BindView(R.id.product_heading_TV)
    TextView productHeadingTV;
    @BindView(R.id.price_rate_TV)
    TextView priceRateTV;
    @BindView(R.id.avatar_name_TV)
    TextView avatarNameTV;
    @BindView(R.id.heart_TV)
    TextView heartTV;
    @BindView(R.id.repost_TV)
    TextView repostTV;
    @BindView(R.id.empty_box_TV)
    TextView emptyBoxTV;
    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, NunitoExtraBold_TTF, CircularStdMedium_TTF, NunitoSemiBold_TTF;
    @BindView(R.id.shop_it_on_nike)
    Button shopItOnNike;
    @BindView(R.id.total_image_count)
    TextView totalImageCount;
    @BindView(R.id.total_image_count_text)
    TextView totalImageCountText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.detailed_product_info_layout, null);
        unbinder = ButterKnife.bind(this, view);
        applyfonts();
        return view;
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        NunitoSemiBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-SemiBold.ttf");
        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        CircularStdMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");

        productHeadingTV.setTypeface(NunitoSemiBold_TTF);
        priceRateTV.setTypeface(NunitoBold_TTF);
        avatarNameTV.setTypeface(NunitoRegular_TTF);

        heartTV.setTypeface(NunitoLight_TTF);
        emptyBoxTV.setTypeface(NunitoLight_TTF);
        repostTV.setTypeface(NunitoLight_TTF);

        messageCount.setTypeface(NunitoRegular_TTF);
        shopItOnNike.setTypeface(NunitoBold_TTF);

        totalImageCount.setTypeface(NunitoLight_TTF);
        totalImageCountText.setTypeface(NunitoLight_TTF);
        heartTV.setTypeface(NunitoLight_TTF);
        repostTV.setTypeface(NunitoLight_TTF);
        emptyBoxTV.setTypeface(NunitoLight_TTF);
        messageCount.setTypeface(NunitoLight_TTF);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.imagemain, R.id.imagev1, R.id.imagev2, R.id.imagev3, R.id.imagev4, R.id.share_icon, R.id.message_count})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imagemain:
//                onOpenDialog();
                openImageDialog();
                break;
            case R.id.imagev1:
                imagemain.setImageResource(R.drawable.second_main_sample);
                break;
            case R.id.imagev2:
                imagemain.setImageResource(R.drawable.main_image);
                break;
            case R.id.imagev3:
                imagemain.setImageResource(R.drawable.second_main_sample);
                break;
            case R.id.imagev4:
//                imagemain.setImageResource(R.drawable.main_image);
//                onOpenDialog();
                openImageDialog();
                break;
            case R.id.share_icon:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new ShareImageToFriends()).addToBackStack(null).commit();
                break;
            case R.id.message_count:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new ProductCommentFragment()).addToBackStack(null).commit();
                ((MainActivity)getActivity()).setBottomNavigationNotVisible();
        }
    }

    private void openImageDialog() {
        ((MainActivity) getActivity()).setBottomNavigationNotVisible();
        getFragmentManager().beginTransaction().replace(R.id.main_container, new DialogProductImageFragment()).addToBackStack(null).commit();
    }

}


