package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.adapter.DialogProductImagesAdapter;

public class DialogProductImageFragment extends Fragment {

    @BindView(R.id.product_images_listview)
    ListView productImagesListview;
    Unbinder unbinder;
    @BindView(R.id.dialog_product_image_back_arrow)
    ImageView dialogProductImageBackArrow;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.image_view_layout, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DialogProductImagesAdapter dialogProductImagesAdapter = new DialogProductImagesAdapter(getActivity());
        productImagesListview.setAdapter(dialogProductImagesAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.dialog_product_image_back_arrow)
    public void onViewClicked() {
        ((MainActivity)getActivity()).bottomNavigationVisibility();
        getFragmentManager().beginTransaction().replace(R.id.main_container, new DetailedProductInfoFragment()).addToBackStack(null).commit();

    }
}
