package corner.ezimax.com.newstaggered.fragment;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;

public class EditProfileFragment extends Fragment {

    @BindView(R.id.account_name_data_ET)
    EditText account_name_data_ET;
    Unbinder unbinder;
    @BindView(R.id.account_name_data_ET_edit)
    TextView editNameButton;
    @BindView(R.id.email_Et)
    EditText email_Et;
    @BindView(R.id.email_ET_edit)
    TextView email_ET_edit;
    @BindView(R.id.password_ET)
    EditText password_ET;
    @BindView(R.id.password_Et_edit)
    TextView password_Et_edit;
    @BindView(R.id.edit_profile_heading_TV)
    TextView editProfileHeadingTV;
    @BindView(R.id.update_your_preferences_TV)
    TextView updateYourPreferencesTV;
    @BindView(R.id.privacy_TV)
    TextView privacyTV;
    @BindView(R.id.public_TV)
    TextView publicTV;
    @BindView(R.id.account_heading_TV)
    TextView accountHeadingTV;
    @BindView(R.id.account_name_field_TV)
    TextView accountNameFieldTV;
    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, NunitoExtraBold_TTF, CircularStdMedium_TTF;
    @BindView(R.id.date_of_birth)
    TextView dateOfBirth;
    @BindView(R.id.date_of_birth_Et)
    EditText dateOfBirthET;
    @BindView(R.id.date_of_birth_ET_edit)
    TextView date_of_birth_ET_edit;
    @BindView(R.id.phone_number_TV)
    TextView phoneNumberTV;
    @BindView(R.id.phone_number_ET)
    EditText phoneNumberET;
    @BindView(R.id.phone_number_ET_edit)
    TextView phoneNumberETEdit;
    @BindView(R.id.credential_heading_TV)
    TextView credentialHeadingTV;
    @BindView(R.id.email_TV)
    TextView emailTV;
    @BindView(R.id.password_TV)
    TextView passwordTV;
    @BindView(R.id.others_heading_TV)
    TextView othersHeadingTV;
    @BindView(R.id.notification_TV)
    TextView notificationTV;
    @BindView(R.id.support_TV)
    TextView supportTV;
    @BindView(R.id.terms_of_services_TV)
    TextView termsOfServicesTV;
    @BindView(R.id.logout)
    TextView logout;


    @Override
    public void onStart() {
        super.onStart();
//        account_name_data_ET.setFocusableInTouchMode(false);
//        dateOfBirthET.setFocusableInTouchMode(false);
//        phoneNumberET.setFocusableInTouchMode(false);
//        email_Et.setFocusableInTouchMode(false);
//        password_ET.setFocusableInTouchMode(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_profile_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        applyfonts();
        return view;
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        CircularStdMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");

        editProfileHeadingTV.setTypeface(CircularStdMedium_TTF);
        updateYourPreferencesTV.setTypeface(NunitoRegular_TTF);
        privacyTV.setTypeface(NunitoBold_TTF);
        publicTV.setTypeface(NunitoRegular_TTF);
        accountHeadingTV.setTypeface(NunitoBold_TTF);
        accountNameFieldTV.setTypeface(NunitoRegular_TTF);
        account_name_data_ET.setTypeface(NunitoBold_TTF);

        dateOfBirth.setTypeface(NunitoRegular_TTF);
        dateOfBirthET.setTypeface(NunitoBold_TTF);
        date_of_birth_ET_edit.setTypeface(NunitoRegular_TTF);

        phoneNumberTV.setTypeface(NunitoRegular_TTF);
        phoneNumberET.setTypeface(NunitoBold_TTF);
        phoneNumberETEdit.setTypeface(NunitoRegular_TTF);

        credentialHeadingTV.setTypeface(NunitoBold_TTF);
        emailTV.setTypeface(NunitoRegular_TTF);
        email_Et.setTypeface(NunitoBold_TTF);
        email_ET_edit.setTypeface(NunitoRegular_TTF);

        passwordTV.setTypeface(NunitoRegular_TTF);
        password_ET.setTypeface(NunitoBold_TTF);
        password_Et_edit.setTypeface(NunitoRegular_TTF);

        othersHeadingTV.setTypeface(NunitoBold_TTF);
        notificationTV.setTypeface(NunitoRegular_TTF);
        supportTV.setTypeface(NunitoRegular_TTF);
        termsOfServicesTV.setTypeface(NunitoRegular_TTF);
        logout.setTypeface(NunitoRegular_TTF);
        editNameButton.setTypeface(NunitoRegular_TTF);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        account_name_data_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
//                    //do what you want on the press of 'done'
//                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    assert imm != null;
//                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//                }
//                return false;
//            }
//        });
        requestFocus(account_name_data_ET);
//        phoneNumberET.setHint(phoneNumberET.getHint() + "");
        account_name_data_ET.setFocusableInTouchMode(true);
//        showKeyboard(getContext(),phoneNumberET);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.account_name_data_ET_edit)
    public void onViewClicked() {
        requestFocus(account_name_data_ET);
        account_name_data_ET.setHint(account_name_data_ET.getHint() + "");
        account_name_data_ET.setFocusableInTouchMode(true);
        showKeyboard(getContext(),account_name_data_ET);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick({R.id.date_of_birth_ET_edit, R.id.phone_number_ET_edit, R.id.email_ET_edit, R.id.password_Et_edit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.date_of_birth_ET_edit:
                requestFocus(dateOfBirthET);
                dateOfBirthET.setHint(dateOfBirthET.getHint() + "");
                dateOfBirthET.setFocusableInTouchMode(true);
                showKeyboard(getContext(),dateOfBirthET);
                break;
            case R.id.phone_number_ET_edit:
                requestFocus(phoneNumberET);
                phoneNumberET.setHint(phoneNumberET.getHint() + "");
                phoneNumberET.setFocusableInTouchMode(true);
                showKeyboard(getContext(),phoneNumberET);
                break;
            case R.id.email_ET_edit:
                requestFocus(email_Et);
                email_Et.setHint(email_Et.getHint() + "");
                email_Et.setFocusableInTouchMode(true);
                showKeyboard(getContext(),email_Et);
                break;
            case R.id.password_Et_edit:
                requestFocus(password_ET);
                password_ET.setHint(password_ET.getHint() + "");
                password_ET.setFocusableInTouchMode(true);
                showKeyboard(getContext(),password_ET);
                break;
                default:
                    Toast.makeText(getContext(), "default", Toast.LENGTH_SHORT).show();
        }
    }


    public static void showKeyboard(Context context,EditText editText) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                editText.getApplicationWindowToken(),InputMethodManager.SHOW_FORCED, 0);
    }
}
