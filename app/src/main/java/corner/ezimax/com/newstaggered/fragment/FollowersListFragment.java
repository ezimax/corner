package corner.ezimax.com.newstaggered.fragment;

import android.animation.ArgbEvaluator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.transition.TransitionManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.FollowerFollowingAdapter;

public class FollowersListFragment extends Fragment {

    @BindView(R.id.viewPager_followers)
    ViewPager viewPagerFollowers;
    @BindView(R.id.rootFrame)
    LinearLayout rootFrame;
    Unbinder unbinder;
    ArgbEvaluator argbEvaluator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.followers_list_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        TransitionManager.beginDelayedTransition(rootFrame);
        argbEvaluator = new ArgbEvaluator();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int devWidth = displayMetrics.widthPixels;

        setUpPagerAdapter();
        viewPagerFollowers.setClipToPadding(false);
        viewPagerFollowers.setPageMargin(-devWidth / 2);

        viewPagerFollowers.addOnPageChangeListener(pageChangeListener);
        viewPagerFollowers.setPageTransformer(true, pageTransformer);

        return view;
    }


    private void setUpPagerAdapter() {

        List<Integer> data = Arrays.asList(0, 1);
        FollowerFollowingAdapter adapter = new FollowerFollowingAdapter(data, getActivity());
        viewPagerFollowers.setAdapter(adapter);
    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    ViewPager.PageTransformer pageTransformer = new ViewPager.PageTransformer() {
        @Override
        public void transformPage(View page, float position) {

            if (position < -1) { // [-Infinity,-1)
                Toast.makeText(getContext(), "position is less than -1", Toast.LENGTH_SHORT).show();
            } else if (position <= 1) { // [-1,1]

                if (position >= -1 && position < 0) {

                    LinearLayout chat_list_view_holder = page.findViewById(R.id.following_list_view_holder);
                    TextView chat_layout_heading = page.findViewById(R.id.following_layout_heading);

                    if (chat_list_view_holder != null && chat_layout_heading != null) {

                        chat_layout_heading.setTextColor((Integer) argbEvaluator.evaluate(-2 * position, getResources().getColor(R.color.black)
                                , getResources().getColor(R.color.grey)));

//                        uberEcoTv.setTextSize(16 + 4 * position);
                        chat_list_view_holder.setX((page.getWidth() * position));
                    }

                } else if (position >= 0 && position <= 1) {

                    TextView contact_layout_heading = page.findViewById(R.id.followers_layout_heading);
                    LinearLayout contacts_list_view_holder = page.findViewById(R.id.followers_list_view_holder);

                    if (contact_layout_heading != null && contacts_list_view_holder != null) {

                        contact_layout_heading.setTextColor((Integer) new ArgbEvaluator().evaluate((1 - position), getResources().getColor(R.color.grey)
                                , getResources().getColor(R.color.black)));

//                        uberPreTv.setTextSize(12 + 4 * (1 - position));
                        contacts_list_view_holder.setX(contacts_list_view_holder.getLeft() + (page.getWidth() * (position)));
                    }
                }
            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
