package corner.ezimax.com.newstaggered.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Objects;

import LoginView.LoginView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.ApiClient.RetrofitClient;
import corner.ezimax.com.newstaggered.BuildConfig;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.apiResponse.LoginResponse;
import corner.ezimax.com.newstaggered.interfaces.ApiInterface;
import corner.ezimax.com.newstaggered.model.LoginModel;
import corner.ezimax.com.newstaggered.singleton.ServerDetails;
import presenter.LoginPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment implements LoginView {

    @BindView(R.id.login_ET)
    EditText loginET;
    @BindView(R.id.password_ET)
    EditText passwordET;
    @BindView(R.id.login_BTN)
    Button loginBTN;
    @BindView(R.id.regiter_BTN)
    Button regiterBTN;
    Unbinder unbinder;
    ApiInterface apiInterface;
    private static final String TAG = "LoginFragment";
    @BindView(R.id.rootView)
    LinearLayout rootView;
    LoginPresenter loginPresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (BuildConfig.DEBUG) {
            loginET.setText("kappu@a.com");
            passwordET.setText("11111");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.login_BTN, R.id.regiter_BTN})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_BTN:
                hideKeyboard();
                final String email_id = loginET.getText().toString();
                final String password = passwordET.getText().toString();
                final HashMap<String, String> loginBody = new HashMap<>();

                loginBody.put("emailId", email_id);
                loginBody.put("password", password);

                apiInterface.userLogin(ServerDetails.getInstance().token, "application/json", loginBody).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(@NonNull Call<Object> call, @NonNull final Response<Object> response) {
                        log("response" + response.body());
                        if (response.isSuccessful()) {
                            Gson gson = new Gson();
                            String responseBody = gson.toJson(response.body());
                            LoginResponse loginResponse = gson.fromJson(responseBody, LoginResponse.class);
                            ServerDetails.getInstance().token = loginResponse.getToken();
                            String success_message = loginResponse.getMessage();
                            String userId = loginResponse.getResult().getUserId();
                            String emaidId = loginResponse.getResult().getEmailId();
                            String contactNumber = loginResponse.getResult().getMobile();
                            boolean Auth = loginResponse.isAuth();
                            log("" + ServerDetails.getInstance().token);
                            loginPresenter = new LoginModel(LoginFragment.this);
                            loginPresenter.performLogin(email_id, password);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {

                    }
                });

                break;
            case R.id.regiter_BTN:
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_container, new RegisterFragment()).addToBackStack(null).commit();
                break;
        }
    }

    private void log(String message) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, " --- " + " " + message);
    }


    @Override
    public void loginSuccess() {
        getFragmentManager().beginTransaction()
                .replace(R.id.main_container, new ProductFragment()).addToBackStack(null).commit();
        ((MainActivity) getActivity()).bottomNavigationVisibility();
    }

    @Override
    public void loginError() {
        Snackbar snackbar = Snackbar.make(rootView, "Login Error", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void loginValidation() {
        Snackbar snackbar = Snackbar.make(rootView, "Please enter username and password", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
