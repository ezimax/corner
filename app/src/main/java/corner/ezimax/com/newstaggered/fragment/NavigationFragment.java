package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.navigation.ColorAdapter;
import corner.ezimax.com.newstaggered.adapter.navigation.PriceAdapter;
import corner.ezimax.com.newstaggered.adapter.navigation.SizeAdapter;

public class NavigationFragment extends Fragment {

    @BindView(R.id.color_right_arrow)
    ImageView colorRightArrow;
    @BindView(R.id.color_list_listview)
    ListView colorListListview;
    @BindView(R.id.left_nav_drawer)
    LinearLayout leftNavDrawer;
    Unbinder unbinder;

    ArrayList<String> colorNamesList;
    ArrayList<String> sizeNamesList;
    ArrayList<String> priceList;
    @BindView(R.id.drop_color_linearLayout)
    RelativeLayout dropColorLinearLayout;
    @BindView(R.id.size_list_listview)
    ListView sizeListListview;
    @BindView(R.id.size_right_arrow)
    ImageView sizeRightArrow;
    @BindView(R.id.drop_size_linearLayout)
    RelativeLayout dropSizeLinearLayout;
    @BindView(R.id.price_right_arrow)
    ImageView priceRightArrow;
    @BindView(R.id.drop_price_linearLayout)
    RelativeLayout dropPriceLinearLayout;
    @BindView(R.id.price_list_listview)
    ListView priceListListview;
    @BindView(R.id.dresigner_linearLayout_holder)
    RelativeLayout dresignerLinearLayoutHolder;
    @BindView(R.id.refine_Tv)
    TextView refineTv;
    private int color_bar_drop_count = 0;
    private int size_bar_drop_count = 0;
    private int price_bar_drop_count = 0;
    @BindView(R.id.navigation_drawer_heading)
    TextView navigation_drawer_heading;
    @BindView(R.id.color_heading_strip)
    TextView color_heading_strip;
    @BindView(R.id.size_heading_strip)
    TextView size_heading_strip;
    @BindView(R.id.price_heading_strip)
    TextView price_heading_strip;
    @BindView(R.id.designer_heading_strip)
    TextView designer_heading_strip;
    @BindView(R.id.category_heading_strip)
    TextView category_heading_strip;
    @BindView(R.id.new_item_heading_strip)
    TextView new_item_heading_strip;
    @BindView(R.id.price_range_heading_strip)
    TextView price_range_heading_strip;

    Typeface CircularStdBold_TTF, CircularStdBlack_TTF;
    FragmentCall fragmentCall;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.navigation_drawer_layout, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        staticDate();

        ColorAdapter navigationColorAdapter = new ColorAdapter(getActivity(), colorNamesList);
        colorListListview.setAdapter(navigationColorAdapter);

        SizeAdapter sizeAdapter = new SizeAdapter(getActivity(), sizeNamesList);
        sizeListListview.setAdapter(sizeAdapter);

        PriceAdapter priceAdapter = new PriceAdapter(getActivity(), priceList);
        priceListListview.setAdapter(priceAdapter);

        applyfonts();
    }

    private void staticDate() {
        colorNamesList = new ArrayList<>();
        colorNamesList.add("Black");
        colorNamesList.add("Red");
        colorNamesList.add("Blue");
        colorNamesList.add("Green");
        colorNamesList.add("Yellow");
        colorNamesList.add("Purple");

        sizeNamesList = new ArrayList<>();
        sizeNamesList.add("XXXL");
        sizeNamesList.add("XXL");
        sizeNamesList.add("XL");
        sizeNamesList.add("L");
        sizeNamesList.add("M");
        sizeNamesList.add("S");

        priceList = new ArrayList<>();
        priceList.add("Lowest");
        priceList.add("Highest");
    }

    private void applyfonts() {
        CircularStdBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Bold.otf");
        navigation_drawer_heading.setTypeface(CircularStdBold_TTF);

        CircularStdBlack_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Black.otf");
        color_heading_strip.setTypeface(CircularStdBlack_TTF);

        size_heading_strip.setTypeface(CircularStdBlack_TTF);
        price_heading_strip.setTypeface(CircularStdBlack_TTF);
        designer_heading_strip.setTypeface(CircularStdBlack_TTF);
        category_heading_strip.setTypeface(CircularStdBlack_TTF);
        new_item_heading_strip.setTypeface(CircularStdBlack_TTF);
        price_range_heading_strip.setTypeface(CircularStdBlack_TTF);
        Typeface Nunito_Bold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        refineTv.setTypeface(Nunito_Bold_TTF);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.drop_color_linearLayout, R.id.drop_size_linearLayout, R.id.drop_price_linearLayout, R.id.refine_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.drop_color_linearLayout:
                colorStripFunctionality();
                break;
            case R.id.drop_size_linearLayout:
                sizeStripFunctionality();
                break;
            case R.id.drop_price_linearLayout:
                priceStripFunctionality();
                break;
            case R.id.refine_button:
                fragmentCall.call();
//                getFragmentManager().beginTransaction().replace(R.id.main_container, new PeopleProductFragment()).addToBackStack(null).commit();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentCall = (FragmentCall)context;
        }catch (Exception  e){

        }
    }

    public interface FragmentCall {
        public void call();
    }

    private void priceStripFunctionality() {
        price_bar_drop_count = ++price_bar_drop_count;
        if (price_bar_drop_count == 1) {
            priceRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow_navigation_vector));
            priceListListview.setVisibility(View.VISIBLE);
        }
        if (price_bar_drop_count == 2) {
            priceListListview.setVisibility(View.GONE);
            priceRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.right_arrow_vector));
            price_bar_drop_count = 0;
        }
    }

    private void sizeStripFunctionality() {
        size_bar_drop_count = ++size_bar_drop_count;
        if (size_bar_drop_count == 1) {
            sizeRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow_navigation_vector));
            sizeListListview.setVisibility(View.VISIBLE);
        }
        if (size_bar_drop_count == 2) {
            sizeListListview.setVisibility(View.GONE);
            sizeRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.right_arrow_vector));
            size_bar_drop_count = 0;
        }
    }

    private void colorStripFunctionality() {
        color_bar_drop_count = ++color_bar_drop_count;
        if (color_bar_drop_count == 1) {
            colorListListview.setVisibility(View.VISIBLE);
            colorRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow_navigation_vector));
        }
        if (color_bar_drop_count == 2) {
            colorListListview.setVisibility(View.GONE);
            colorRightArrow.setImageDrawable(getResources().getDrawable(R.drawable.right_arrow_vector));
            color_bar_drop_count = 0;
        }
    }
}
