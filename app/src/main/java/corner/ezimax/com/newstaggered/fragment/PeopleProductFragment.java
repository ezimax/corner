package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.PeopleDataAdapter;
import corner.ezimax.com.newstaggered.adapter.ProductDataAdapter;
import corner.ezimax.com.newstaggered.model.SearchPeopleModel;
import corner.ezimax.com.newstaggered.model.SearchProductsModel;

public class PeopleProductFragment extends Fragment {

    @BindView(R.id.people_button_holder_LL)
    LinearLayout peopleButtonHolderLL;
    @BindView(R.id.product_button_holder_LL)
    LinearLayout productButtonHolderLL;
    @BindView(R.id.people_product_list_view)
    ListView peopleProductListView;
    Unbinder unbinder;
    @BindView(R.id.people_TV)
    TextView peopleTV;
    @BindView(R.id.product_TV)
    TextView productTV;
    @BindView(R.id.search_view_ET)
    EditText searchViewET;
    private int maleImages[] = {R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136};
    private int femaleImages[] = {R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size, R.drawable.female_avatar_big_size};
    private ArrayList<String> product_name, product_price;
    Typeface Circular_std_bold, Nunito_Bold_ttf;
    ArrayList<SearchProductsModel> productModels = new ArrayList<>();
    ArrayList<SearchPeopleModel> peopleModels = new ArrayList<>();
    private ProductDataAdapter productDataAdapter;
    private PeopleDataAdapter peopleDataAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.people_product_layout, null);
        unbinder = ButterKnife.bind(this, view);
        Circular_std_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Bold.otf");
        peopleTV.setTypeface(Circular_std_bold);
        productTV.setTypeface(Circular_std_bold);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        staticData();
        productModelData();
        peopleModelData();
        productClick();
        searchViewET.setTextColor(Color.parseColor("#212121"));

        Nunito_Bold_ttf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        searchViewET.setTypeface(Nunito_Bold_ttf);


        searchViewET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (productTV.getCurrentTextColor() == Color.parseColor("#181818")) {
                    filterProduct(s.toString());
                } else {
                    filterPeople(s.toString());
                }
            }
        });
    }

    private void productModelData() {
        productModels.addAll(Collections.singleton(new SearchProductsModel("John Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("kapil Doe", R.drawable.flipkat_one)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("John Doe", R.drawable.jabong_sample_img1)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("fraaz Doe", R.drawable.new_sample_four)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("John Doe", R.drawable.new_sample_five)));


        productModels.addAll(Collections.singleton(new SearchProductsModel("abc Doe", R.drawable.jabong_sample_img3)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("John Doe", R.drawable.new_sample_three)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("anything Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("John Doe", R.drawable.flipkart_two)));
        productModels.addAll(Collections.singleton(new SearchProductsModel("zzz Doe", R.drawable.jabong_sample_img2)));

    }

    private void peopleModelData() {
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("John Doe", R.drawable.new_sample_two)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("kapil Doe", R.drawable.flipkat_one)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("John Doe", R.drawable.jabong_sample_img1)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("fraaz Doe", R.drawable.new_sample_four)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("John Doe", R.drawable.new_sample_five)));


        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("abc Doe", R.drawable.jabong_sample_img3)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("John Doe", R.drawable.new_sample_three)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("anything Doe", R.drawable.new_sample_two)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("John Doe", R.drawable.flipkart_two)));
        peopleModels.addAll(Collections.singleton(new SearchPeopleModel("zzz Doe", R.drawable.jabong_sample_img2)));

    }


    private void filterProduct(String query) {

        final ArrayList<SearchProductsModel> filteredModelList = new ArrayList<>();
        for (SearchProductsModel item : productModels) {
            if (item.getProductName().toLowerCase().contains(query.toLowerCase())) {
                filteredModelList.addAll(Collections.singleton(item));
            }
        }

        productDataAdapter.setFilter(filteredModelList);
    }

    public void filterPeople(String query) {
        final ArrayList<SearchPeopleModel> filteredModelList = new ArrayList<>();
        for (SearchPeopleModel item : peopleModels) {
            if (item.getPeopleName().toLowerCase().contains(query.toLowerCase())) {
                filteredModelList.addAll(Collections.singleton(item));
            }
        }
        peopleDataAdapter.setFilter(filteredModelList);
    }

    private void staticData() {
        product_name = new ArrayList<>();
        product_name.add("John Doe");
        product_name.add("Jane Doe");
        product_name.add("John Doe");
        product_name.add("Jane Doe");
        product_name.add("John Doe");
        product_name.add("Jane Doe");
        product_name.add("John Doe");
        product_name.add("Jane Doe");
        product_name.add("John Doe");
        product_name.add("Jane Doe");

        product_price = new ArrayList<>();
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");
        product_price.add("$32.10");

        ProductDataAdapter productDataAdapter = new ProductDataAdapter(getActivity(), productModels, product_price);
        peopleProductListView.setAdapter(productDataAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.people_button_holder_LL, R.id.product_button_holder_LL, R.id.cross})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.people_button_holder_LL:
                peopleClick();
                break;
            case R.id.product_button_holder_LL:
                productClick();
                break;
            case R.id.cross:
                searchViewET.setText("");
                searchViewET.setHint("Search here...");
                break;
        }
    }

    public void productClick() {
        productTV.setTextColor(getResources().getColor(R.color.black_active_button_color));
        peopleTV.setTextColor(getResources().getColor(R.color.grey));
        productDataAdapter = new ProductDataAdapter(getActivity(), productModels, product_price);
        peopleProductListView.setAdapter(productDataAdapter);
    }

    public void peopleClick() {
        peopleTV.setTextColor(getResources().getColor(R.color.black_active_button_color));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        peopleDataAdapter = new PeopleDataAdapter(getActivity(), peopleModels);
        peopleProductListView.setAdapter(peopleDataAdapter);
    }
}
