package corner.ezimax.com.newstaggered.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.activity.MainActivity;
import corner.ezimax.com.newstaggered.adapter.ProductCommentsAdapter;

public class ProductCommentFragment extends Fragment {

    @BindView(R.id.product_comments_listView)
    ListView productCommentsListView;
    @BindView(R.id.send_message_in_product_comment_button)
    ImageView sendMessageButton;
    Unbinder unbinder;
    @BindView(R.id.product_category)
    TextView productCategory;
    @BindView(R.id.product_category_also)
    TextView productCategoryAlso;
    @BindView(R.id.women_gender_type)
    TextView genderType;
    @BindView(R.id.product_price)
    TextView productPrice;
    List<String> message;
    List<String> MessageTimeResponse;
    List<Boolean> initiatorResponse;
    List<Integer> initiatorIdResponse;
    List<Integer> recepientIdResponse;
    private String sent_message;
    @BindView(R.id.type_message_area_ET)
    EditText type_message_area_ET;
    private Date currentTime;
    private ProductCommentsAdapter productCommentsAdapter;
    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, NunitoExtraBold_TTF, CircularStdMedium_TTF, GothamRoundedMedium_TTF, GothamRoundedBook_TTF;
    private String time;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_comment_layout, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        jsonData();
        applyfonts();
        productCommentsAdapter = new ProductCommentsAdapter(getActivity(), message, MessageTimeResponse, initiatorResponse, initiatorIdResponse, recepientIdResponse);
        productCommentsListView.setAdapter(productCommentsAdapter);
    }

    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        CircularStdMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");
        GothamRoundedMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRounded-Medium.otf");
        GothamRoundedBook_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRounded-Book.otf");

        type_message_area_ET.setTypeface(GothamRoundedMedium_TTF);
        productCategory.setTypeface(GothamRoundedMedium_TTF);
        productCategoryAlso.setTypeface(GothamRoundedMedium_TTF);
        genderType.setTypeface(GothamRoundedBook_TTF);
        productPrice.setTypeface(GothamRoundedMedium_TTF);


    }

    private void jsonData() {
        JSONArray jsonArray2 = new JSONArray();
        JSONObject initiator = new JSONObject();
        try {
            initiator.put("Initiator", false);
            initiator.put("InitiatorId", 1);
            initiator.put("RecepientId", 1);
            initiator.put("MessageText", "Hi, sorry for late reply. Yes it’s still" +
                    "available.");
            initiator.put("MessageTime", "08:00 PM");
            initiator.put("MessageImage", "");
            initiator.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject participant = new JSONObject();
        try {
            participant.put("Initiator", false);
            participant.put("InitiatorId", 2);
            participant.put("RecepientId", 2);
            participant.put("MessageText", "Umm… I’m sorry. That won’t do it." +
                    "70 and it’s yours");
            participant.put("MessageTime", "09:40 PM");
            participant.put("MessageImage", "");
            participant.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject initiator1 = new JSONObject();
        try {
            initiator1.put("Initiator", false);
            initiator1.put("InitiatorId", 3);
            initiator1.put("RecepientId", 3);
            initiator1.put("MessageText", "Nice!!");
            initiator1.put("MessageTime", "10:00 PM");
            initiator1.put("MessageImage", "");
            initiator1.put("MessageLink", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsonArray2.put(initiator);
        jsonArray2.put(participant);
        jsonArray2.put(initiator1);

        JSONObject chatObject = new JSONObject();
        try {
            chatObject.put("chatMessage", jsonArray2);
//            try {
//                JSONArray jsonArray = (JSONArray) chatObject.get("chatMessage");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            message = new ArrayList<>();
            MessageTimeResponse = new ArrayList<>();
            initiatorResponse = new ArrayList<>();
            initiatorIdResponse = new ArrayList<>();
            recepientIdResponse = new ArrayList<>();
            JSONArray jsonArray1 = null;
            try {
                jsonArray1 = chatObject.getJSONArray("chatMessage");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsonArray1 != null) {
                for (int i = 0; i < jsonArray1.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jsonArray1.getJSONObject(i);

                        Boolean initiatorData = jsonObject.getBoolean("Initiator");
                        initiatorResponse.add(initiatorData);
                        Integer initiatorID = jsonObject.getInt("InitiatorId");
                        initiatorIdResponse.add(initiatorID);
                        Integer RecepientId = jsonObject.getInt("RecepientId");
                        recepientIdResponse.add(RecepientId);
                        String MessageText = jsonObject.getString("MessageText");
                        message.add(MessageText);
                        String MessageTime = jsonObject.getString("MessageTime");
                        MessageTimeResponse.add(MessageTime);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.chat_back_arrow, R.id.send_message_in_product_comment_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chat_back_arrow:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new DetailedProductInfoFragment()).addToBackStack(null).commit();
                ((MainActivity) getActivity()).bottomNavigationVisibility();
                break;
            case R.id.send_message_in_product_comment_button:

                sent_message = type_message_area_ET.getText().toString();
                DateFormat df = new SimpleDateFormat("h:mm a");
                time = df.format(Calendar.getInstance().getTime());
                type_message_area_ET.setText("");
                type_message_area_ET.setHint(R.string.type_here);

                if (message.size() == initiatorResponse.size() && initiatorResponse.size() == initiatorIdResponse.size() && initiatorIdResponse.size() == MessageTimeResponse.size()
                        ) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (sent_message.matches("")) {
                                Toast.makeText(getActivity(), "Enter comment please", Toast.LENGTH_SHORT).show();
                            } else {
                                message.add(sent_message);
                                initiatorResponse.add(false);
                                initiatorIdResponse.add(3);
                                MessageTimeResponse.add(time);
                                productCommentsAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                }
                break;
        }
    }

    private void log(String message) {
        String TAG = "ProductCommentFragment";
        if (BuildConfig.DEBUG)
            Log.e(TAG, " ************* " + message);
    }
}
