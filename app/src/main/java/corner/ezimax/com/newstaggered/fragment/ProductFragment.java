package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.Utils.SpacesItemDecoration;
import corner.ezimax.com.newstaggered.adapter.ProductAdapter;
import corner.ezimax.com.newstaggered.adapter.TagsAdapter;
import corner.ezimax.com.newstaggered.model.ProductModel;


public class ProductFragment extends Fragment {

    @BindView(R.id.masonry_grid)
    RecyclerView mRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.navToggle)
    ImageView navToggle;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.toolbar)
    CardView toolbar;
    @BindView(R.id.container_toolbar)
    LinearLayout containerToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.drawer_view)
    RelativeLayout drawerView;
    @BindView(R.id.fragment_navigation_drawer_holder)
    LinearLayout fragmentNavigationDrawerHolder;
    public Boolean mSlideState = false;
    boolean navOpenFirts = true;
    @BindView(R.id.close_navigation_drawer_image)
    ImageView close_navigation_drawer_image;
    @BindView(R.id.app_name_in_toolbar)
    TextView appNameInToolbar;
    Typeface NunitoExtraBold_TTF;
    @BindView(R.id.tags__recyclerView)
    RecyclerView tags__recyclerView;
    ArrayList personNames = new ArrayList<>(Arrays.asList("Denim", "Shorts", "Coats", "Sweater", "Jackets", "Vests", "Polos", "Shoes", "Shirts"));
    @BindView(R.id.toolbar_block_holder)
    RelativeLayout toolbar_block_holder;
//    @BindView(R.id.search_edit_text)
//    EditText search_edit_text;
    ProductAdapter adapter;


    ArrayList<ProductModel> productModels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.activity_main, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        modelData();
        applyFonts();
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        adapter = new ProductAdapter(getActivity(), productModels);
        mRecyclerView.setAdapter(adapter);
        SpacesItemDecoration decoration = new SpacesItemDecoration(16);
        mRecyclerView.addItemDecoration(decoration);


//        search_edit_text.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                filter(s.toString());
//            }
//        });
    }

//    private void filter(String query) {
//
//        final ArrayList<ProductModel> filteredModelList = new ArrayList<>();
//        for (ProductModel item : productModels) {
//            if (item.getProductName().toLowerCase().contains(query.toLowerCase())) {
//                filteredModelList.addAll(Collections.singleton(item));
//            }
//        }
//        adapter.setFilter(filteredModelList);
//    }


    private void modelData() {
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.flipkat_one)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.jabong_sample_img1)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_four)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_five)));


        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img3)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_three)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.flipkart_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img2)));

    }


    private void applyFonts() {
        // appNameInToolbar
        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        appNameInToolbar.setTypeface(NunitoExtraBold_TTF);
    }

    public void tagsVisible() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.HORIZONTAL, false);
        tags__recyclerView.setLayoutManager(gridLayoutManager);
        TagsAdapter customAdapter = new TagsAdapter(getActivity(), personNames);
        tags__recyclerView.setAdapter(customAdapter);
        tags__recyclerView.setVisibility(View.VISIBLE);
        drawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    public void onStart() {
        super.onStart();
        ActionBarDrawerToggle chatSectionToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                mSlideState = false;//is Closed
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mSlideState = true;//is Opened
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(chatSectionToggle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.navToggle, R.id.close_navigation_drawer_image, R.id.search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.navToggle:
                if (mSlideState) {
                    drawerLayout.closeDrawer(Gravity.START);
                } else {
                    if (navOpenFirts) {
                        navOpenFirts = false;
                    }
                    drawerLayout.openDrawer(Gravity.START);
                }
                break;
            case R.id.close_navigation_drawer_image:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.search:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new PeopleProductFragment()).addToBackStack(null).commit();
                break;
        }
    }
}
