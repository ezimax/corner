package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.HashMap;

import LoginView.RegisterView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.ApiClient.RetrofitClient;
import corner.ezimax.com.newstaggered.BuildConfig;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.interfaces.ApiInterface;
import corner.ezimax.com.newstaggered.model.RegisterationModel;
import corner.ezimax.com.newstaggered.singleton.ServerDetails;
import presenter.RegisterPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment implements RegisterView {

    @BindView(R.id.first_name_ET)
    EditText firstNameET;
    @BindView(R.id.last_name_ET)
    EditText lastNameET;
    @BindView(R.id.email_Id_ET)
    EditText emailIdET;
    @BindView(R.id.contact_number_ET)
    EditText contactNumberET;
    @BindView(R.id.password_ET)
    EditText passwordET;
    @BindView(R.id.regiter_BTN)
    Button regiterBTN;
    Unbinder unbinder;
    ApiInterface apiInterface;
    private static final String TAG = "RegisterFragment";
    RegisterPresenter registerPresenter;
    @BindView(R.id.rootView)
    LinearLayout rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.registration_fragment_layout, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (BuildConfig.DEBUG) {
            firstNameET.setText("1");
            lastNameET.setText("o");
            emailIdET.setText("a@a.com");
            contactNumberET.setText("2222245454");
            passwordET.setText("11111");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.regiter_BTN)
    public void onViewClicked() {

        final String first_name = firstNameET.getText().toString();
        final String last_name = lastNameET.getText().toString();
        final String email = emailIdET.getText().toString();
        final String contactNumber = contactNumberET.getText().toString();
        final String password = passwordET.getText().toString();

        HashMap<String, String> registerBody = new HashMap<>();
        registerBody.put("firstName", first_name);
        registerBody.put("lastName", last_name);
        registerBody.put("emailId", email);
        registerBody.put("mobile", contactNumber);
        registerBody.put("password", password);

        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        apiInterface.registerUser(ServerDetails.getInstance().token, "application/json", registerBody).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                log("response" + response.body());
                registerPresenter = new RegisterationModel(RegisterFragment.this);
                registerPresenter.performRegistration(first_name, last_name, email, contactNumber, password);


            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void log(String message) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, " ----*****---- " + " " + message);
    }

    @Override
    public void registerSuccess() {
        getFragmentManager().beginTransaction().replace(R.id.main_container, new LoginFragment()).addToBackStack(null).commit();
    }

    @Override
    public void registerError() {

        Snackbar snackbar = Snackbar.make(rootView, "Registeration Error", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void registerValidation() {

        Snackbar snackbar = Snackbar.make(rootView, "Please fill all fields", Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
