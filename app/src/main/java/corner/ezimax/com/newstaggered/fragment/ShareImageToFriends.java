package corner.ezimax.com.newstaggered.fragment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.adapter.shareFriendsImageAdapter;


public class ShareImageToFriends extends Fragment {

    @BindView(R.id.share_screen_back_button)
    ImageView shareScreenBackButton;
    @BindView(R.id.share_image_friends_list_recyclerView)
    RecyclerView shareImageFriendsListRecyclerView;
    Unbinder unbinder;
    int[] shareFriendImage = {R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136, R.drawable.male_big_avatar_116x136};
    List<String> shareFriendsName;
    @BindView(R.id.share_heading_TV)
    TextView shareHeadingTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.share_image_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        Typeface CircularStdMedium_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");
        shareHeadingTV.setTypeface(CircularStdMedium_TTF);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        staticData();
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/CircularStd-Medium.otf");
        shareHeadingTV.setTypeface(typeface);
        initRecyclerView();

    }

    private void staticData() {
        shareFriendsName = new ArrayList<>();
        shareFriendsName.add("Alzea Arafat");
        shareFriendsName.add("John Doe");
        shareFriendsName.add("Alzea Arafat");
        shareFriendsName.add("John Doe");
        shareFriendsName.add("Alzea Arafat");
        shareFriendsName.add("John Doe");
        shareFriendsName.add("Alzea Arafat");
        shareFriendsName.add("John Doe");
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        shareImageFriendsListRecyclerView.setLayoutManager(mLayoutManager);
        shareImageFriendsListRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(), true));
        shareImageFriendsListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        shareFriendsImageAdapter adapter = new shareFriendsImageAdapter(getActivity(), shareFriendsName, shareFriendImage);
        shareImageFriendsListRecyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.share_screen_back_button)
    public void onViewClicked() {
        getFragmentManager().beginTransaction().replace(R.id.main_container, new DetailedProductInfoFragment()).addToBackStack(null).commit();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx() {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
