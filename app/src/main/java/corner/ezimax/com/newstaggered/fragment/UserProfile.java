package corner.ezimax.com.newstaggered.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import corner.ezimax.com.newstaggered.R;
import corner.ezimax.com.newstaggered.Utils.SpacesItemDecoration;
import corner.ezimax.com.newstaggered.adapter.HorizontalImagesAdapter;
import corner.ezimax.com.newstaggered.adapter.ProductAdapter;
import corner.ezimax.com.newstaggered.model.ProductModel;


public class UserProfile extends Fragment {


    @BindView(R.id.collection_tab_content)
    LinearLayout collectionTabContent;
    @BindView(R.id.repost_tab_content)
    LinearLayout repostTabContent;
    Unbinder unbinder;
    @BindView(R.id.casual_collection_products)
    RecyclerView casual_collection_products;
    @BindView(R.id.formal_collection_products)
    RecyclerView formal_collection_products;
    @BindView(R.id.repost_grid)
    RecyclerView repostGridV;
    @BindView(R.id.edit_profile_image)
    ImageView edit_profile_image;
    Typeface NunitoBold_TTF, NunitoRegular_TTF, NunitoLight_TTF, NunitoExtraBold_TTF;
    @BindView(R.id.user_full_name)
    TextView userFullName;
    @BindView(R.id.user_location)
    TextView userLocation;
    @BindView(R.id.user_description)
    TextView userDescription;
    @BindView(R.id.user_collection_count)
    TextView userCollectionCount;
    @BindView(R.id.user_followers_count)
    TextView userFollowersCount;
    @BindView(R.id.user_following_count)
    TextView userFollowingCount;
    @BindView(R.id.user_collection_TV)
    TextView userCollectionTV;
    @BindView(R.id.user_followers_TV)
    TextView userFollowersTV;
    @BindView(R.id.user_following_TV)
    TextView userFollowingTV;
    @BindView(R.id.repost_button_TV)
    TextView repostButtonTV;
    @BindView(R.id.collection_button_TV)
    TextView collectionButtonTV;
    @BindView(R.id.create_new_collection_TV)
    TextView createNewCollectionTV;
    @BindView(R.id.casual_text_TV)
    TextView casualTextTV;
    @BindView(R.id.formal_text_TV)
    TextView formalTextTV;
    ArrayList<ProductModel> productModels = new ArrayList<>();

    @BindView(R.id.create_new_collection_holder_LL)
    LinearLayout create_new_collection_holder_LL;
    @BindView(R.id.toolbar)
    CardView toolbar;

    private int[] imgList = {R.drawable.new_sample_three, R.drawable.new_sample_two, R.drawable.new_sample_one, R.drawable.new_sample_five,
            R.drawable.sample1, R.drawable.new_sample_three, R.drawable.sample2, R.drawable.sample4,
            R.drawable.sample1, R.drawable.new_sample_three};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.user_collection_layout, null);
        unbinder = ButterKnife.bind(this, view);
        applyfonts();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        modelData();
        repostTabContent.setVisibility(View.VISIBLE);
        collectionTabContent.setVisibility(View.GONE);
//        casual_collection_products.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
//
//
//        CasualProductsAdapter adapter = new CasualProductsAdapter(getActivity());
//        casual_collection_products.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        casual_collection_products.setLayoutManager(layoutManager);
        HorizontalImagesAdapter adapter1 = new HorizontalImagesAdapter( imgList,getActivity());
        casual_collection_products.setAdapter(adapter1);
//        SpacesItemDecoration decoration = new SpacesItemDecoration(16);
//        casual_collection_products.addItemDecoration(decoration);


        formal_collection_products.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));


//        CasualProductsAdapter adapter1 = new CasualProductsAdapter(getActivity());
//        formal_collection_products.setAdapter(adapter1);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        formal_collection_products.setLayoutManager(layoutManager1);
        HorizontalImagesAdapter adapter3 = new HorizontalImagesAdapter( imgList,getActivity());
        formal_collection_products.setAdapter(adapter3);
//        SpacesItemDecoration decoration1 = new SpacesItemDecoration(16);
//        formal_collection_products.addItemDecoration(decoration1);

        repostGridV.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        ProductAdapter adapter2 = new ProductAdapter(getActivity(),productModels);
        repostGridV.setAdapter(adapter2);
        SpacesItemDecoration decoration2 = new SpacesItemDecoration(16);
        repostGridV.addItemDecoration(decoration2);
    }


    private void modelData() {
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.flipkat_one)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.jabong_sample_img1)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_four)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_five)));


        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img3)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.new_sample_three)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.new_sample_two)));
        productModels.addAll(Collections.singleton(new ProductModel("John Doe", R.drawable.flipkart_two)));
        productModels.addAll(Collections.singleton(new ProductModel("Jane Doe", R.drawable.jabong_sample_img2)));

    }


    private void applyfonts() {
        NunitoBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Bold.ttf");
        userFullName.setTypeface(NunitoBold_TTF);

        NunitoRegular_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Regular.ttf");
        userLocation.setTypeface(NunitoRegular_TTF);

        NunitoLight_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-Light.ttf");
        userDescription.setTypeface(NunitoLight_TTF);


        userCollectionCount.setTypeface(NunitoBold_TTF);
        userFollowersCount.setTypeface(NunitoBold_TTF);
        userFollowingCount.setTypeface(NunitoBold_TTF);


        NunitoExtraBold_TTF = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-ExtraBold.ttf");
        userCollectionTV.setTypeface(NunitoExtraBold_TTF);
        userFollowersTV.setTypeface(NunitoExtraBold_TTF);
        userFollowingTV.setTypeface(NunitoExtraBold_TTF);

        repostButtonTV.setTypeface(NunitoRegular_TTF);
        collectionButtonTV.setTypeface(NunitoRegular_TTF);

        createNewCollectionTV.setTypeface(NunitoBold_TTF);
        casualTextTV.setTypeface(NunitoBold_TTF);
        formalTextTV.setTypeface(NunitoBold_TTF);



    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Optional
    @OnClick({R.id.repost_button, R.id.collection_button, R.id.edit_profile_image, R.id.followings_text, R.id.followings_value,R.id.followers_value,R.id.followers_text,R.id.collection_settings_image,R.id.collection_settings_image_2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.repost_button:
                toolbar.setCardElevation(2);
                collectionTabContent.setVisibility(View.GONE);
                repostTabContent.setVisibility(View.VISIBLE);
                break;
            case R.id.collection_button:
                toolbar.setCardElevation(0);
                collectionTabContent.setVisibility(View.VISIBLE);
                repostTabContent.setVisibility(View.GONE);
                break;
            case R.id.edit_profile_image:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new EditProfileFragment()).addToBackStack(null).commit();
                break;
            case R.id.followings_value:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followings_text:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followers_value:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.followers_text:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new FollowersListFragment()).addToBackStack(null).commit();
                break;
            case R.id.collection_settings_image:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionSettings()).addToBackStack(null).commit();
                break;
            case R.id.collection_settings_image_2:
                getFragmentManager().beginTransaction().replace(R.id.main_container, new CollectionSettings()).addToBackStack(null).commit();
                break;

        }
    }

}
