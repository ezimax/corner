package corner.ezimax.com.newstaggered.interfaces;


import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {

    @HEAD("verifyUser")
    Call<Void> getUserToken();

    @POST("loginuser")
    Call<Object> userLogin(@Header("x-access-token") String token,
                           @Header("Content-Type") String value,
                           @Body HashMap<String, String> stringHashMap);

    @POST("signUpUser")
    Call<Object> registerUser(@Header("x-access-token") String token,
                              @Header("Content-Type") String value,
                              @Body HashMap<String, String> hashMap);
}
