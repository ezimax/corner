package corner.ezimax.com.newstaggered.model;


import android.text.TextUtils;

import LoginView.LoginView;
import presenter.LoginPresenter;

public class LoginModel implements LoginPresenter {

    private LoginView loginView;

    public LoginModel(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void performLogin(String email, String password) {
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            loginView.loginValidation();
        } else {
            if (email.equals("kappu@a.com") && password.equals("11111")) {
                loginView.loginSuccess();
            } else {
                loginView.loginError();
            }
        }

    }
}
