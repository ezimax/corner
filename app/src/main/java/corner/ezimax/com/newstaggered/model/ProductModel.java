package corner.ezimax.com.newstaggered.model;

public class ProductModel {

    private String productName;
    private int productImages;

    public ProductModel(String productName, int productImages) {
        this.productName = productName;
        this.productImages = productImages;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductImages() {
        return productImages;
    }

    public void setProductImages(int productImages) {
        this.productImages = productImages;
    }
}
