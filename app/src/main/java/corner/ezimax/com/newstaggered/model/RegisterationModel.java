package corner.ezimax.com.newstaggered.model;

import android.text.TextUtils;

import LoginView.RegisterView;
import presenter.RegisterPresenter;

public class RegisterationModel implements RegisterPresenter {

    private RegisterView registerView;

    public RegisterationModel(RegisterView registerView) {
        this.registerView = registerView;
    }

    @Override
    public void performRegistration(String firstName, String lastName, String emailId, String contactNumber, String password) {

        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(emailId) || TextUtils.isEmpty(contactNumber) || TextUtils.isEmpty(password)) {
            registerView.registerValidation();
        } else {
            registerView.registerSuccess();

        }
    }
}
