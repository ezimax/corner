package corner.ezimax.com.newstaggered.model;

public class SearchPeopleModel {
    private String peopleName;
    private int peopleImages;


    public SearchPeopleModel(String peopleName, int peopleImages) {
        this.peopleName = peopleName;
        this.peopleImages = peopleImages;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public int getPeopleImages() {
        return peopleImages;
    }

    public void setPeopleImages(int peopleImages) {
        this.peopleImages = peopleImages;
    }
}
