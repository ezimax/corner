package corner.ezimax.com.newstaggered.model;

public class SearchProductsModel {

    private String productName;
    private int productImages;


    public SearchProductsModel(String produuctName, int productImages) {
        this.productName = produuctName;
        this.productImages = productImages;
    }

    public String getProductName() {
        return productName;
    }

    public void setProduuctName(String productName) {
        this.productName = productName;
    }

    public int getProductImages() {
        return productImages;
    }

    public void setProductImages(int productImages) {
        this.productImages = productImages;
    }
}
