package corner.ezimax.com.newstaggered.singleton;


public class ServerDetails {
    private static final ServerDetails Instance = new ServerDetails();


    private ServerDetails() {
    }

    public String baseUrl = "http://192.168.1.80:8090/api/";
    public String token;

    public static ServerDetails getInstance() {
        return Instance;
    }
}
