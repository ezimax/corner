package presenter;

public interface RegisterPresenter {
    void performRegistration(String firstName,String lastName,String emailId,String contactNumber,String password);
}
